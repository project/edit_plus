export const formatterPropertyMapping = {
  text_summary_or_trimmed: 'summary',
};

/**
 * Get property.
 *
 * @todo Describe the problem space around this!
 *
 * @param elementInfo
 * @returns {*|string}
 */
export const getProperty = (elementInfo) => {
  const formatter = elementInfo.widget;

  const propertyEvent = new CustomEvent('formatterPropertyMapping', {
    detail: {
      formatter: formatter,
      property: formatterPropertyMapping[formatter] ?? elementInfo.mainProperty ?? 'value',
    },
    bubbles: true,
    cancelable: true
  });

  window.dispatchEvent(propertyEvent);

  return propertyEvent.detail.property;
}
