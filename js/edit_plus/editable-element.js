import * as formatterPropertyMap from './formatter-property-map.js';

/**
 * Editable Element.
 *
 * This class is a state object that represents the relationship of the currently
 * edited page element to the form element.
 */
export class EditableElement {

  /**
   * Info
   *
   * All the details about this editable element.
   *
   * @type {{elementId, langcode: *, fieldName: *, mainProperty: *, delta: *, entityTypeId: *, entityId: *, widget: *, viewMode: *, fieldType: *, widget: *}}
   */
  info = null;

  /**
   * The inline editing field plugin for this EditableElement.
   * @see field-plugin-manager.js
   *
   * @type {FieldPluginBase}
   */
  plugin = null;

  constructor(id) {
    this.info = this.parseMarkupId(id);
  }

  /**
   * Create from form item.
   *
   * @param element
   *   An element within a form item wrapper.
   *
   * @returns {EditableElement}
   *   A new EditableElement instance from a form item or any element from
   *   within a form item wrapper.
   */
  static createFromFormItem(element) {
    // Find the page element
    let pageElement;
    let formItem = element.closest('.edit-plus-form-item');
    if (formItem) {
      pageElement =  document.querySelector('[data-edit-plus-page-element-id="' + formItem.dataset.editPlusFormItemId + '"]');
    } else {
      const formItemWrapperId = element.closest('[data-edit-plus-form-item-wrapper-id]').dataset.editPlusFormItemWrapperId;
      // Let's assume a delta of 0 and get the page element.
      const pageElementId = formItemWrapperId.split('::');
      pageElementId.splice(3, 0, '0');
      pageElement = document.querySelector('[data-edit-plus-page-element-id="' + pageElementId.join('::') + '"]');
    }

    const editableElement = new EditableElement(pageElement.dataset.editPlusId);
    editableElement.setWidget();
    return editableElement;
  }

  /**
   * Set widget.
   *
   * Once the form is loaded the widget needs to be set on the EditableElement.
   */
  setWidget() {
    this.info.widget = this.getFormItemWrapper().dataset.editPlusFormItemWidget;
    this.plugin = Drupal.EditPlus.FieldPluginManager.getPlugin(this);
  }

  /**
   * Get form.
   *
   * @returns {Element}
   * The form for the EditableElement.
   */
  getForm() {
    const id = [
      this.info.entityTypeId,
      this.info.entityId,
    ].join('::');
    return document.querySelector('[data-edit-plus-form-id="' + id + '"]');
  }

  /**
   * Get handle type.
   *
   * @returns {string}
   *   Returns a string of either form_item or wrapper.
   *
   *   Users can config whether a field swaps the form item or the entire form
   *   wrapper into the page for editing.
   */
  getHandleType() {
    return this.getFormItemWrapper()?.dataset.editPlusHandle ?? 'form_item';
  }

  /**
   * Handle is wrapper.
   *
   * @see getFormItemHandle
   *
   * @returns {boolean}
   *   Returns true if the handle is wrapper. false if the handle is form_item.
   */
  handleIsWrapper() {
    const wrapper = this.getFormItemWrapper();
    const elementToUseAsHandle = wrapper.dataset.editPlusHandle ?? 'form_item';
    // @todo This doesn't account for getFormItemHandle() gracefully falling back to the wrapper.
    return elementToUseAsHandle === 'wrapper';
  }

  /**
   * Get page element.
   *
   * @returns {Element}
   *   The rendered markup on the page for the EditableElement.
   */
  getPageElement() {
    return document.querySelector('[data-edit-plus-id="' + this.info.elementId + '"]');
  }

  getPageElementWrapper() {
    const id = [
      this.info.entityTypeId,
      this.info.entityId,
      this.info.fieldName,
      this.info.mainProperty,
    ].join('::');
    return document.querySelector('[data-edit-plus-field-value-wrapper="' + id + '"]');
  }

  getPageElementHandle() {
    if (this.getHandleType() === 'wrapper') {
      return this.getPageElementWrapper();
    }
    return document.querySelector('[data-edit-plus-id="' + this.info.elementId + '"]');
  }

  /**
   * Get Form Item ID.
   *
   * @returns {string}
   *   The form item ID.
   */
  getFormItemId() {
    return [
      this.info.entityTypeId,
      this.info.entityId,
      this.info.fieldName,
      this.info.delta,
      formatterPropertyMap.getProperty(this.info),
    ].join('::');
  }

  /**
   * Get form item.
   *
   * @returns {Element}
   * The form item for the EditableElement.
   */
  getFormItem() {
    return document.querySelector('[data-edit-plus-form-item-id="' + this.getFormItemId() + '"]');
  }

  /**
   * Get form item wrapper.
   *
   * @returns {Element}
   *   The form item wrapper for the EditableElement.
   */
  getFormItemWrapper() {
    const id = [
      this.info.entityTypeId,
      this.info.entityId,
      this.info.fieldName,
      formatterPropertyMap.getProperty(this.info),
    ].join('::');
    return document.querySelector('[data-edit-plus-form-item-wrapper-id="' + id + '"]');
  }

  /**
   * Get form item handle.
   *
   * @returns {Element}
   *   The handle for the form element that will be swapped in for inline editing.
   *   @see getHandleType.
   */
  getFormItemHandle() {
    if (this.getHandleType() === 'wrapper') {
      return this.getFormItemWrapper();
    }

    try {
      return document.querySelector('[data-edit-plus-form-item-id="' + this.getFormItemId() + '"]').closest('.edit-plus-form-item');
    } catch (e) {
      console.warn(`The ${this.info.fieldName} field is configured to use the form item as a handle, but no form item markup was found. Try configuring ${this.info.fieldName} to use the wrapper as a handle instead.`);
      const wrapper = this.getFormItemWrapper();
      if (wrapper) {
        console.info(`Falling back to the wrapper handle for ${this.info.fieldName}.`);
        return wrapper;
      }
      throw e;
    }
  }

  /**
   * Get form input.
   *
   * @returns {Element}
   *   The input element for the EditableElement.
   */
  getFormItemInputs() {
    return this.getFormItemHandle().querySelectorAll('input, textarea, select');
  }


  getSizedPlaceholderId() {
    return 'sized-placeholder-' + this.info.elementId.replace(/::/g, '-');
  }

  /**
   * Parse markup ID.
   *
   * @param id
   *   The Entity Plus ID on the rendered field value.
   *
   * @returns {{elementId, langcode: *, fieldName: *, mainProperty: *, delta: *, entityTypeId: *, entityId: *, widget: *, viewMode: *, fieldType: *}}
   *   The elementInfo object.
   */
  parseMarkupId(id) {
    const parts = id.split('::');
    let elementInfo = {
      entityTypeId: parts[0],
      bundle: parts[1],
      entityId: parts[2],
      fieldName: parts[3],
      delta: parts[4],
      langcode: parts[5],
      fieldType: parts[6],
      fieldFormatter: parts[7],
      mainProperty: parts[8],
      elementId: id,
    };
    elementInfo.viewMode = document.querySelector(`[data-navigation-plus-entity-wrapper="${elementInfo.entityTypeId}::${elementInfo.entityId}::${elementInfo.bundle}"]`).dataset.navigationPlusViewMode;

    return elementInfo;
  }

}
