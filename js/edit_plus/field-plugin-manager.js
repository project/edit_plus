import * as entityReferenceAutocomplete from './plugins/entity-reference-autocomplete.js';
import * as fieldPluginBase from './plugins/field-plugin-base.js';
import * as inlineTextarea from './plugins/inline-editor.js';
import * as defaultPlugin from './plugins/default.js';
import * as textfield from './plugins/textfield.js';
import * as mediaPlugin from './plugins/media.js';

/**
 * Field plugin manager.
 *
 * Field plugins are used throughout the UI for things like focus, blur, etc.
 */
class FieldPluginManager {
  constructor() {
    this.plugins = {};
  }

  /**
   * Register plugin.
   *
   * @param plugin
   *  The field plugin.
   */
  registerPlugin(plugin) {
    if (plugin instanceof fieldPluginBase.FieldPluginBase) {
      this.plugins[plugin.constructor.name] = plugin;
      plugin.init();
    } else {
      console.error('Failed to register plugin: ', plugin);
    }
  }

  /**
   * Get plugin.
   *
   * @param EditableElement
   *   The element being edited.   The element being edited.
   * @returns {FieldPluginBase}
   *   The field plugin based on the field format.
   */
  getPlugin = (EditableElement) => {
    let pluginForElement = null;

    Object.values(this.plugins).forEach(plugin => {
      plugin.supportedWidgets.forEach(widget => {
        if (EditableElement.info.widget === widget && plugin.applies(EditableElement)) {
          pluginForElement = plugin;
        }
      });
    });

    if (!pluginForElement) {
      return this.plugins.DefaultPlugin;
    }

    return pluginForElement;
  }
}

export default function ($ = jQuery, Drupal, once) {

  /**
   * Initialize the field plugins.
   */
  entityReferenceAutocomplete.default($, Drupal, once);
  textfield.default($, Drupal, once);
  inlineTextarea.default($, Drupal, once);
  defaultPlugin.default($, Drupal, once);
  mediaPlugin.default($, Drupal, once);

  /**
   * Register the field plugins.
   */
  Drupal.EditPlus.FieldPluginManager = new FieldPluginManager();
  const registerPluginsEvent = new CustomEvent('EditPlusFieldManager.RegisterPlugins', {
    detail: {
      manager: Drupal.EditPlus.FieldPluginManager,
    },
    bubbles: true,
    cancelable: true
  });
  window.dispatchEvent(registerPluginsEvent);

}
