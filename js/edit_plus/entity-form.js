import * as editableElement from './editable-element.js';

/**
 * Get form.
 *
 * @param EditableElement
 *   The element info object.
 * @returns {Promise<unknown>}
 *   A promise that retrieves either an Entity Form, or a Layout Builder (block)
 *   entity update form. If the form has already been fetched then it reveals
 *   the appropriate form.
 */
export const getForm = (EditableElement) => {
  // Hide/cache previously loaded forms.
  document.querySelectorAll('.edit-plus-form').forEach(form => {
    form.classList.add('navigation-plus-hidden');
    form.removeAttribute('data-offset-right');
  });
  // Return the requested form if we already have it.
  const formExists = EditableElement.getForm();
  if (formExists) {
    return new Promise((resolve, reject) => {
      formExists.classList.remove('navigation-plus-hidden');
      formExists.setAttribute('data-offset-right', '');
      resolve({status: 'existing_form'});
    });
  }

  // Fetch a new form.
  const pageElement = EditableElement.getPageElement();
  const layoutBuilderBlock = pageElement.closest('.block-layout-builder');
  if (layoutBuilderBlock) {
    // Use the regular entity form for field blocks.
    // @todo Maybe add a different field-block class that we don't need to search for...
    let isFieldBlock = false;
    for (let i=0; i < layoutBuilderBlock.classList.length; i++) {
      isFieldBlock = layoutBuilderBlock.classList[i].includes('field-block');
      if (isFieldBlock) {
        break;
      }
    }
    if (!isFieldBlock) {
      return getLayoutBuilderEntityForm(EditableElement);
    }
  }
  return getEntityForm(EditableElement);
}

/**
 * Get Layout Builder Entity Form.
 *
 * @param EditableElement
 *   The element info object.
 *
 * @returns {Promise<unknown>}
 *   A promise to retrieve the form.
 */
const getLayoutBuilderEntityForm = (EditableElement) => {
  return new Promise((resolve, reject) => {
    // Figure out the URL parameters based on the markup.
    const pageElement = EditableElement.getPageElement();
    const storageIds = jQuery(pageElement).parents('[data-layout-builder-section-storage-id]');
    const sectionStorage = storageIds[storageIds.length - 1].dataset
    const storageType = sectionStorage.layoutBuilderSectionStorageType;
    const storageId = sectionStorage.layoutBuilderSectionStorageId;
    const region = pageElement.closest('[data-layout-builder-region]').dataset.layoutBuilderRegion;
    const block = pageElement.closest('[data-layout-builder-block-uuid]');
    const sectionDelta = block.closest('[data-layout-builder-section-delta]').dataset.layoutBuilderSectionDelta;
    const blockUuid = block.dataset.layoutBuilderBlockUuid;
    let nestedStoragePath = '';
    for (let i = storageIds.length - 2; i >= 0; i--) {
      const layoutBlock = storageIds[i].closest('[data-layout-builder-layout-block]');
      const section = layoutBlock.closest('[data-layout-builder-section-delta]');
      if (nestedStoragePath) {
        nestedStoragePath += '&';
      }
      nestedStoragePath += `${section.dataset.layoutBuilderSectionDelta}&${layoutBlock.dataset.layoutBuilderBlockUuid}`;
    }
    nestedStoragePath = nestedStoragePath ? '/' + nestedStoragePath : nestedStoragePath;
    let url = Drupal.url(`edit-plus/update/block/${storageType}/${storageId}/${sectionDelta}/${region}/${blockUuid}${nestedStoragePath}`);

    EditableElement.info.ajaxReturnForm = true;
    let ajax = Drupal.NavigationPlus.ModePluginBase.ajax({
      url: url,
      type: 'GET',
      dataType: 'text',
      submit: EditableElement.info,
      progress: {
        type: 'fullscreen',
        message: Drupal.t('Loading form...')
      },
      error: error => {
        console.error(error.responseText);
        reject(error);
      },
      success: (response, status) => {
        Promise.resolve(
          response.forEach(r => {
            // @todo Is setting the selector here a hack? Consider making a renderer like Drupal\Core\Render\MainContent\AjaxRenderer.
            if (r.command === 'insert' && r.method === null && r.data && r.data.includes('edit-plus-form')) {
              r.method = 'append';
              r.selector = '#navigation-plus-right-sidebar';
            }
          }),
        )
          .then(() => {
            Drupal.Ajax.prototype.success.call(ajax, response, status).then(() => {
              resolve({ status: 'new_form' });
            });
          });
      }
    });
    ajax.execute();
    EditableElement.info.ajaxReturnForm = false;
  });
}

/**
 * Get Entity Form.
 *
 * @param EditableElement
 *   The element info object.
 *
 * @returns {Promise<unknown>}
 *   A promise to retrieve the form.
 */
const getEntityForm = (EditableElement) => {
  return new Promise((resolve, reject) => {

    EditableElement.info.ajaxReturnForm = true;
    let ajax = Drupal.NavigationPlus.ModePluginBase.ajax({
      url: Drupal.url('edit-plus/entity-form/' + EditableElement.info.entityTypeId + '/' + EditableElement.info.entityId),
      type: 'POST',
      dataType: 'text',
      submit: EditableElement.info,
      progress: {
        type: 'fullscreen',
        message: Drupal.t('Loading form...')
      },
      error: error => {
        console.error(error.responseText);
        reject(error);
      },
      success: (response, status) => {
        Promise.resolve(
          Drupal.Ajax.prototype.success.call(ajax, response, status)
        ).then(() => {
          resolve({status: 'new_form'});
        });
      }
    });
    ajax.execute();
    EditableElement.info.ajaxReturnForm = false;
  });
}


/**
 * Flag that the page is currently being updated.
 *
 * @see Drupal.EditPlus.notUpdating
 *
 * @type {boolean}
 */
export let editPlusIsUpdating = false;

/**
 * Update tempstore.
 *
 * @param EditableElement
 *   The form item whose form needs updating.
 * @param isEmpty
 *   Whether the field has been emptied or not. Emptied fields are moved to the
 *   sidebar.
 * @param form
 *   Pass the form element directly when an EditableElement isn't available,
 *   but you still need to submit the form like when a sidebar form item changes.
 * @param onlyUpdateElement
 *   Pass the element to update when an EditableElement isn't available,
 *   but you still need to submit the form like when a sidebar form item changes.
 */
export const updateTempstore = async (EditableElement, isEmpty, form = null, onlyUpdateElement = null) => {
  if (updateTempstore.alreadyUpdating) {
    return;
  }
  if (!EditableElement && Drupal.EditPlus.CurrentlyEditingElement) {
    // We are saving something other than an EditableElement while an
    // EditableElement is focused. Probably something in the sidebar. Let's
    // blur the CurrentlyEditingElement to get any changes from it.
    // Flag that we are already updating as blur can call updateTempstore again.
    updateTempstore.alreadyUpdating = true;
    try {
      await Drupal.EditPlus.CurrentlyEditingElement.plugin.blur();
    } finally {
      updateTempstore.alreadyUpdating = false;
    }
  }

  if (!form) {
    form = EditableElement.getFormItemWrapper().closest('form');
  }

  // Move emptied fields to the sidebar.
  if (EditableElement && isEmpty) {
    form.querySelector('#empty-field').value = EditableElement.info.fieldName;
  }

  // Update the hidden view mode input in order to persist the view mode across
  // multiple form submissions as it could be changing in this update.
  let viewMode;
  if (EditableElement) {
    // I think we can standardize on getting the view mode from the form > entity
    // wrapper relationship detailed in the else, but we might as well get it
    // from EditableElement when it's available as it's a tiny bit faster.
    viewMode = EditableElement.info.viewMode;

    // Disable editing the item while it's being updated.
    const pageElementWrapper = EditableElement.getPageElementWrapper();
    pageElementWrapper.classList.add('disabled-updating');
    // Add an AJAX throbber to indicate the item is being updated.
    const throbber = Drupal.theme.ajaxProgressThrobber(Drupal.t('Updating...'));
    pageElementWrapper.insertAdjacentHTML('beforeend', throbber);
    setTimeout(() => {
      const progress = document.querySelector('.disabled-updating .ajax-progress');
      if (progress) {
        const backgroundColor = getBackgroundColor(pageElementWrapper);
        progress.style.backgroundColor = backgroundColor;
        progress.style.color = getTextColorForBackground(backgroundColor);
        progress.style.opacity = '1';
      }
    }, 50);
    // Flag to hide the submit handler ajax throbber in the sidebar.
    document.querySelector('body').classList.add('element-ajax-throbbing');
    // Flag to only update the rendered output of this field, not the entire
    // entity. This allows users to continue editing other items on the page.
    form.querySelector('.edit-plus-only-update-element').value = EditableElement.getPageElement().dataset.editPlusId;
  } else {
    // EditableElement won't be available when submitting the form via EditPlusEntityFormAutoSave.
    // @todo review if this entityWrapper getting works. It seems like it shouldn't.
    const entityWrapper = document.querySelector('[data-navigation-plus-entity-wrapper^="' + form.closest('.edit-plus-form').dataset.editPlusFormId + '"]');
    viewMode = entityWrapper.dataset.navigationPlusViewMode;
    // Flag to update the render output of the entire entity.
    form.querySelector('.edit-plus-only-update-element').value = onlyUpdateElement;
  }
  // Update the hidden view mode input in order to persist the view mode across
  // multiple form submissions as it could be changing in this update.
  form.querySelector('.edit-plus-view-mode').value = viewMode;


  // Click the update button (formerly the save submit).
  const updateButton = form.querySelector('.edit-plus-update-button');
  jQuery(updateButton).mousedown();
  editPlusIsUpdating = true;
}

/**
 * Edit+ is done updating.
 *
 * AJAX callback that flags that the update is complete.
 */
jQuery.fn.editPlusIsDoneUpdating = () => {
  editPlusIsUpdating = false;
};

/**
 * Get background color.
 *
 * @param element
 *   The element to get the background color for.
 *
 * @returns {string}
 *   The inherited background color of this element.
 */
export const getBackgroundColor = (element) => {
  let currentElement = element;
  while (currentElement) {
    const bgColor = window.getComputedStyle(currentElement).backgroundColor;
    if (bgColor !== 'rgba(0, 0, 0, 0)' && bgColor !== 'transparent') {
      return bgColor;
    }
    currentElement = currentElement.parentElement;
  }

  return 'white';
}

/**
 * Get text color for background.
 *
 * We don't want to have black text on a black background or other similarly
 * un-contrasty situations.
 *
 * @param backgroundColor
 *   The background color the text will appear on.
 *
 * @returns {string}
 *   white or black, which would be a better choice given the background color.
 */
export const getTextColorForBackground = (backgroundColor) => {
  // Extract the RGB values from the string.
  const rgb = backgroundColor.match(/\d+/g);
  const [r, g, b] = rgb ? [parseInt(rgb[0]), parseInt(rgb[1]), parseInt(rgb[2])] : [255, 255, 255];
  // Calculate the luminance of the color.
  const a = [r, g, b].map(v => {
    v /= 255;
    return v <= 0.03928 ? v / 12.92 : Math.pow((v + 0.055) / 1.055, 2.4);
  });
  const luminance = a[0] * 0.2126 + a[1] * 0.7152 + a[2] * 0.0722;

  return luminance < 0.5 ? 'white' : 'black';
}

/**
 * Edit plus update markup
 *
 * This is an AJAX method called after updating the tempstore.
 */
jQuery.fn.EditPlusUpdateMarkup = (selector, content, updatedElementId) => {
  if (updatedElementId) {
    // Let's update only the element that had changes so users can continue to
    // edit other fields.
    const EditableElement = new editableElement.EditableElement(updatedElementId);
    const contentElement = document.createElement('div');
    contentElement.innerHTML = content;
    const elementToUpdateId = EditableElement.getPageElementWrapper().dataset.editPlusFieldValueWrapper;
    const elementToUpdateSelector = '[data-edit-plus-field-value-wrapper="' + elementToUpdateId + '"]';
    const updatedElement = contentElement.querySelector(elementToUpdateSelector);
    const element = document.querySelector(elementToUpdateSelector);
    if (updatedElement) {
      element.replaceWith(updatedElement)
    } else {
      // The element was emptied out. Let's remove it.
      element.remove();
    }
    document.querySelector('body').classList.remove('element-ajax-throbbing');
  } else {
    // Update/replace the whole thing.
    const entityWrapper = document.querySelector(selector);
    entityWrapper.outerHTML = content;
  }
  Drupal.EditPlus.EnableEditMode();
  // Scenario: you have two forms loaded which both have CKEditors inside.
  // when you change one then click to change the other a chain of events happens.
  // updateTempstore clicks the save (ajax update) button > beforeSerialize
  // detaches behaviors > all CKEditors are removed > then the CKEditor you
  // are now editing doesn't have a CKEditor attached. Let's just call
  // attachBehaviors again here to get the editor since it is idempotent.
  Drupal.attachBehaviors();
}

/**
 * Show field with errors.
 *
 * AJAX callback for when the form is submitted, but there are errors. Re-edit
 * the field that had errors.
 *
 * @param selector
 *   The CSS selector of the form item or form item wrapper that has an error
 *   and needs to be edited.
 */
jQuery.fn.ShowFieldWithErrors = (selector) => {
  const element = document.querySelector(selector);
  const EditableElement = editableElement.EditableElement.createFromFormItem(element);
  EditableElement.plugin.edit(EditableElement);
  // Remove throbbers.
  document.querySelector('body').classList.remove('element-ajax-throbbing');
  document.querySelector('.ajax-progress-throbber').remove();
  EditableElement.getPageElementHandle().classList.remove('disabled-updating');
}

(($, Drupal, once) => {

  /**
   * Edit+ entity form auto save.
   *
   * Auto saves form items in the sidebar when the user changes its value.
   */
  Drupal.behaviors.EditPlusEntityFormAutoSave = {
    attach: (context, settings) => {
      once('EditPlusEntityFormAutoSave', '[data-edit-plus-auto-submit]', context).forEach(formItem => {
        formItem.addEventListener('change', e => {
          const input = e.target;
          const onlyUpdateElement = input.dataset.editPlusUpdateElement ?? null;
          updateTempstore(null, false, input.closest('form'), onlyUpdateElement);
        });
      });
    }
  };
})(jQuery, Drupal, once);

