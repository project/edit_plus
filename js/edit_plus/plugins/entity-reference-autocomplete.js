import * as defaultPlugin from './default.js';

export default function ($ = jQuery, Drupal, once, dropZones) {

  /**
   * Entity reference label field plugin.
   */
  class EntityReferenceAutocompletePlugin extends defaultPlugin.DefaultPlugin {

    supportedWidgets = ['entity_reference_autocomplete_tags', 'entity_reference_autocomplete'];

    getFormItemInputs(EditableElement) {
      return EditableElement.getFormItemWrapper().querySelectorAll('input');
    }

    getFocusedSelectors() {
      let selectors = super.getFocusedSelectors();
      selectors.push('.ui-widget');
      return selectors;
    }

  }

  /**
   * Register the entity reference label field plugin.
   */
  window.addEventListener('EditPlusFieldManager.RegisterPlugins', e => {
    e.detail.manager.registerPlugin(new EntityReferenceAutocompletePlugin());
  });

}
