import * as entityForm from '../entity-form.js';
import * as fieldPluginBase from './field-plugin-base.js';

/**
 * Default field plugin.
 */
export class DefaultPlugin extends fieldPluginBase.FieldPluginBase {

  focusElement(EditableElement) {
    this.getInputValue(EditableElement).then(values => {
      // Remember the original values so we can tell if there's changes before
      // updating when the form item looses focus.
      this.originalValue = values;
    });
    this.getFormItemInputs(EditableElement)[0].focus();
  }

  blurElement(EditableElement) {
    return this.getInputValue(EditableElement).then(values => {
      this.replaceFormItemWithPageElement(EditableElement);
      // The values have changed.
      if (JSON.stringify(this.originalValue) !== JSON.stringify(values)) {
        entityForm.updateTempstore(EditableElement, values.length === 0);
      }
      this.originalValue = null;
    });
  }

  getFormItemInputs(EditableElement) {
    return EditableElement.getFormItemInputs();
  }

  /**
   * Get input value(s).
   *
   * @param EditableElement
   *   The editable element.
   *
   * @returns {Promise<{}|*[]>}
   *   A promise because we may need to wait on a CKEditor to be attached before
   *   getting input changes.
   */
  async getInputValue(EditableElement) {
    const inputs = this.getFormItemInputs(EditableElement);
    if (inputs[0].type === 'checkbox') {
      let checkboxes = [];
      for (let i=0; i < inputs.length; i++) {
        const value = inputs[i].checked ? inputs[i].value : null;
        if (value) {
          checkboxes.push(value);
        }
      }
      return checkboxes;
    }
    else {
      // If there's a ckeditor here it won't be initialized on the first pass,
      // then when the values are compared to see if there are changes, there will
      // always be changes on the first go. Let's wait for the ckeditor to load.
      const waitForCkeditor = (ckeditorId) =>{
        return new Promise((resolve, reject) => {
          const ckeditor = Drupal.CKEditor5Instances.get(ckeditorId);
          if (ckeditor) {
            resolve(ckeditor);
          } else {
            let count = 0;
            const limit = 10;
            const interval = setInterval(() => {
              const instance = Drupal.CKEditor5Instances.get(ckeditorId);
              if (instance) {
                clearInterval(interval);
                resolve(instance);
              }
              if (count >= limit) {
                clearInterval(interval);
                reject(new Error(`Invalid value for ${ckeditorId}`));
              }
            }, 10);
          }
        });
      }

      let values = [];
      for (let i=0; i < inputs.length; i++) {
        let value;
        if (inputs[i].hasAttribute('data-ckeditor5-id')) {
          const ckeditorId = inputs[i].dataset.ckeditor5Id;
          const ckeditor = await waitForCkeditor(ckeditorId);
          value = ckeditor.getData();
        } else {
          value = inputs[i].value;
        }
        if (value) {
          values.push(value);
        }
      }
      return values;
    }
  }

}

export default function ($ = jQuery, Drupal, once, dropZones) {

  /**
   * Register the default field plugin.
   */
  window.addEventListener('EditPlusFieldManager.RegisterPlugins', e => {
    e.detail.manager.registerPlugin(new DefaultPlugin());
  });

}
