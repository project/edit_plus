import * as ckeditor5EditorInline from '../../../assets/vendor/ckeditor5/editor-inline.js'; // Do not remove!
import * as core from '../copied_from_core/copied-from-core.js';
import * as editableElement from '../editable-element.js';
import * as fieldPluginBase from './field-plugin-base.js';
import * as entityForm from '../entity-form.js';


export default function ($ = jQuery, Drupal, once, dropZones) {

  /**
   * InlineEditor field plugin.
   */
  class InlineEditor extends fieldPluginBase.FieldPluginBase {

    /**
     * Supported widgets.
     *
     * @type {[string]}
     */
    supportedWidgets = [
      'text_textarea',
      'text_textarea_with_summary',
    ];

    /**
     * Original data.
     *
     * The original input so we can determine if it's been changed.
     *
     * @type {Map<any, any>}
     */
    originalData = new Map();

    focusElement(EditableElement) {
      const formItem = EditableElement.getFormItem();
      const sourceElement = formItem.querySelector('.edit-plus-inline-edit');
      this.setEditorTextColorBasedOnTheElement(sourceElement, EditableElement);

      const ckeditor5Id = sourceElement.dataset.ckeditor5Id;
      if (ckeditor5Id) {
        const editor = Drupal.EditPlus.Ckeditor5Instances.get(ckeditor5Id);
        if (editor) {
          // Normally you'd call editor.focus() here, but it's not playing
          // nice with Safari. Let's manually fire the event.
          setTimeout(() => {
            editor.ui.focusTracker.isFocused = true;
            editor.editing.view.document.fire('focus');
          }, 0);
        } else {
          // The editor hasn't been built yet because the form was just loaded.
          // Remember that this element needs focus and focus it after the editor
          // has been attached.
          // @see attachEditor.
          Drupal.EditPlus.FocusInlineEditor = ckeditor5Id;
        }
      } else {
        // This is a text area.
        EditableElement.getFormItemInputs()[0].focus();
      }

      // Remember what the original data was so that we can update the tempstore
      // if it has changed.
      this.originalData.set(EditableElement.info.elementId, this.getInputValue(formItem));
    }

    // Use whatever the original font color was while editing.
    setEditorTextColorBasedOnTheElement(sourceElement, EditableElement) {
      sourceElement.classList.add('text-color-from-element');
      document.documentElement.style.setProperty('--text-color-from-element', window.getComputedStyle(EditableElement.getPageElement()).color);
    }

    blurElement(EditableElement) {
      const formItem = EditableElement.getFormItem();
      const originalData = this.originalData.get(EditableElement.info.elementId);
      const data = this.getInputValue(formItem);

      this.replaceFormItemWithPageElement(EditableElement);
      if (data !== originalData) {
        entityForm.updateTempstore(EditableElement, data.length === 0);
      }
      return Promise.resolve();
    }

    revealAncillary(EditableElement) {
      if (!EditableElement.handleIsWrapper()) {
        const formItemWrapper = EditableElement.getFormItemWrapper();
        const form = formItemWrapper.closest('form');
        form.insertBefore(formItemWrapper, form.firstChild);
        formItemWrapper.classList.add('edit-plus-focused');
        formItemWrapper.classList.remove('edit-plus-hidden');
      }
    }

    getFocusedSelectors() {
      let selectors = super.getFocusedSelectors();
      selectors.push('.ck-body-wrapper');
      return selectors;
    }

    /**
     * Get data.
     *
     * Gets the user input from either the CKEditor or a textarea.
     *
     * @param formItem
     *   The form item to retrieve data from.
     *
     * @returns {*}
     *   The user input.
     */
    getInputValue(formItem) {
      const ckeditor5Id = formItem.querySelector('.edit-plus-inline-edit').dataset.ckeditor5Id;
      if (ckeditor5Id) {
        const editor = Drupal.EditPlus.Ckeditor5Instances.get(ckeditor5Id);
        if (Drupal.EditPlus.FocusInlineEditor === ckeditor5Id) {
          // The editor was the first thing that was clicked and hasn't been
          // attached to the form yet. this.originalData will be set once the
          // editor has been attached. @see attachEditor
          return null;
        } else {
          return editor.getData();
        }
      } else {
        const textArea = formItem.querySelector('textarea');
        return textArea.value;
      }
    }


    isValid(e = null) {
      const EditableElement = Drupal.EditPlus.CurrentlyEditingElement;

      const formItem = EditableElement.getFormItem();
      const textArea = EditableElement.getFormItemInputs()[0];
      const pageElement = EditableElement.getPageElement();

      const originalData = this.originalData.get(EditableElement.info.elementId);
      const data = this.getInputValue(formItem);

      if (data !== originalData) {
        pageElement.innerHTML = textArea.innerText = data;
      }

      return super.isValid(e);
    }

    validationMessageHandle() {
      const formItem = Drupal.EditPlus.CurrentlyEditingElement.getFormItem();

      const ckeditor5Id = formItem.querySelector('.edit-plus-inline-edit').dataset.ckeditor5Id;
      if (ckeditor5Id) {
        const editor = Drupal.EditPlus.Ckeditor5Instances.get(ckeditor5Id);
        return editor.sourceElement;
      } else {
        return formItem.querySelector('textarea');
      }
    }

    clearValidationMessageOnUserInput (validationMessageHandle) {
      const removeValidationMessage = this.removeValidationMessage;
      const formItem = Drupal.EditPlus.CurrentlyEditingElement.getFormItem();

      const ckeditor5Id = formItem.querySelector('.edit-plus-inline-edit').dataset.ckeditor5Id;
      if (ckeditor5Id) {
        const editor = Drupal.EditPlus.Ckeditor5Instances.get(ckeditor5Id);
        // Attach an 'input' event listener to the CKEditor instance.
        editor.model.document.on('change:data', () => {
          removeValidationMessage(editor.sourceElement);
        });
      } else {
        // Listen for user input.
        formItem.querySelector('textarea').addEventListener('input', e => {
          removeValidationMessage(e.target);
        });
      }
    }

  }

  /**
   * Register the InlineEditor field plugin.
   */
  window.addEventListener('EditPlusFieldManager.RegisterPlugins', e => {
    e.detail.manager.registerPlugin(new InlineEditor());
  });

  /**
   * The Inline CKEditor instances.
   */
  Drupal.EditPlus.Ckeditor5Instances = new Map();

  /**
   * Focus Inline Editor
   *
   * The scenario is that a user clicks on an editable element > the form is
   * retrieved > the element is focused, but actually the CKEditor hasn't been
   * asynchronously attached yet, so this is a flag to the attacher that when
   * this CKEditor is attached it needs focus immediately.
   */
  Drupal.EditPlus.FocusInlineEditor = null;

  /**
   * Create Inline Editors.
   */
  Drupal.behaviors.EditPlusInlineEditors = {
    attach: (context, settings) => {
      once('EditPlusInlineEdit', '[data-inline-editor-for]', context).forEach(formatSelector => {

        // Attach the Inline Editor or reveal the text area.
        const textAreaId = '#' + formatSelector.dataset.inlineEditorFor;
        const markupElement = document.querySelector(textAreaId).closest('.edit-plus-form-item').querySelector('.edit-plus-inline-edit');
        Drupal.behaviors.EditPlusInlineEditors.showInputElement(markupElement, formatSelector.value);

        // Listen for Editor format changes.
        formatSelector.addEventListener('change', e => {
          // Ignore the filter module changes on page load.
          if (e.isTrusted) {
            const formItem = document.querySelector('#' + e.currentTarget.dataset.inlineEditorFor).closest('.edit-plus-form-item');
            const EditableElement = editableElement.EditableElement.createFromFormItem(formItem);
            const textarea = formItem.querySelector('textarea');
            const inlineEditElement = formItem.querySelector('.edit-plus-inline-edit');
            const ckeditor5Id = inlineEditElement.dataset.ckeditor5Id;

            // Update the textarea and inlineEditElement (markupElement) before
            // switching formats to prevent data loss.
            if (ckeditor5Id) {
              const editor = Drupal.EditPlus.Ckeditor5Instances.get(ckeditor5Id);
              if (editor) {
                textarea.value = editor.getData();
              }
            } else {
              inlineEditElement.innerHTML = textarea.value;
            }

            // Remove old editor if it exists.
            if (ckeditor5Id) {
              let destroyPromise = Drupal.behaviors.EditPlusInlineEditors.detachEditor(e.currentTarget, inlineEditElement.dataset.ckeditor5Id);
              destroyPromise.then(() => {
                Drupal.behaviors.EditPlusInlineEditors.attachEditorWithNewFormat(EditableElement, markupElement, formatSelector.value);
              });
            } else {
              Drupal.behaviors.EditPlusInlineEditors.attachEditorWithNewFormat(EditableElement, markupElement, formatSelector.value);
            }
          }
        });
      });
    },
    attachEditorWithNewFormat: (EditableElement, markupElement, format) => {
      once.remove('EditPlusAttachEditor', markupElement);
      Drupal.behaviors.EditPlusInlineEditors.showInputElement(markupElement, format);
      EditableElement.plugin.focus(EditableElement);
    },

    /**
     * Show input element.
     *
     * Either reveals the textarea or builds the Inline Editor based on the
     * field formatter.
     *
     * @param element
     *   The form element to attach the editor to.
     *
     * @param formatId
     *   The field formatter ID.
     */
    showInputElement: (element, formatId) => {
      const format = drupalSettings.editor.formats[formatId];
      if (format) {
        Drupal.behaviors.EditPlusInlineEditors.attachEditor(element, format);
      } else {
        // No editor, reveal the textarea.
        const textarea = element.closest('.edit-plus-form-item').querySelector('textarea');
        textarea.classList.remove('edit-plus-hidden');
        element.classList.add('edit-plus-hidden');
      }
    },

    /**
     * Attach Editor.
     *
     * Attaches the CKEditor Inline Editor to some markup in a form item.
     *
     * @param element
     *   The markup in a form item.
     * @param format
     *   The field formatter ID.
     */
    attachEditor: (element, format) => {
      once('EditPlusAttachEditor', element).forEach(element => {
        // Hide the textarea if it hasn't been already.
        element.classList.remove('edit-plus-hidden');
        element.closest('.edit-plus-form-item').querySelector('textarea').classList.add('edit-plus-hidden');

        // Process the field formatter config.
        const { editorInline } = CKEditor5;
        let {
          toolbar,
          plugins,
          config: pluginConfig,
          language,
        } = format.editorSettings;

        // @todo The source editing plugin is not supported in the InlineEditor.
        // It is currently hidden with CSS, but let's do something that accounts for
        // block separators so you don't get 2 of them together ||.
        // plugins = plugins.filter(item => item !== 'sourceEditing.SourceEditing');

        const extraPlugins = core.selectPlugins(plugins);
        const config = {
          extraPlugins,
          toolbar,
          language,
          ...core.processConfig(pluginConfig),
        };

        // Add an Inline Editor
        const id = core.setElementId(element);
        const { InlineEditor } = editorInline;

        InlineEditor
          .create(element, config)
          .then((editor) => {
            Drupal.EditPlus.Ckeditor5Instances.set(id, editor);
            const formItem = editor.sourceElement.closest('.edit-plus-form-item');

            // Does this editor need focus right away? If the CKEditor was the
            // first thing that was clicked that loaded the form, we should focus
            // the editor as soon as it's created.
            if (id === Drupal.EditPlus.FocusInlineEditor) {
              // Normally you'd call editor.focus() here, but it's not playing
              // nice with safari. Let's manually fire the event.
              editor.ui.focusTracker.isFocused = true;
              editor.editing.view.document.fire('focus');
              Drupal.EditPlus.FocusInlineEditor = null;

              // Remember the editors original state so we can detect if it has
              // changed and trigger a tempstore update.
              const EditableElement = editableElement.EditableElement.createFromFormItem(formItem);
              const plugin = EditableElement.plugin;
              plugin.originalData.set(EditableElement.info.elementId, plugin.getInputValue(formItem));

            }
          })
          .catch(err => {
            console.error(err.stack);
          });
      });
    },

    /**
     * Detach Editor.
     *
     * @param formatSelector
     *   The format selector select list whose editor needs removing.
     * @param ckEditorId
     *   The CKEditor ID.
     *
     * @returns {*}
     *   A CKEditor destroy promise.
     */
    detachEditor: (formatSelector, ckEditorId) => {
      const editor = Drupal.EditPlus.Ckeditor5Instances.get(ckEditorId);
      if (editor) {
        const data = editor.getData();
        const inlineEditElement = editor.sourceElement;
        once.remove('EditPlusInlineEdit', '[data-editor-for]', formatSelector.parentElement);
        return editor.destroy()
          .then(() => {
            inlineEditElement.removeAttribute('data-ckeditor5-id');
            inlineEditElement.innerHTML = data;
          });
      }
    },
  };
}
