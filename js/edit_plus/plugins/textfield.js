import * as defaultPlugin from './default.js';

export default function ($ = jQuery, Drupal, once, dropZones) {

  /**
   * Text field plugin.
   */
  class TextfieldPlugin extends defaultPlugin.DefaultPlugin {

    supportedWidgets = ['string_textfield', 'string_textarea'];

    replacePageElementWithFormItem(EditableElement) {
      const pageElement = EditableElement.getPageElementHandle();
      pageElement.setAttribute('contenteditable', true);
      pageElement.classList.add('edit-plus-text-editing');
    }

    focusElement(EditableElement) {
      this.getInputValue(EditableElement).then(values => {
        this.originalValue = values;
      });
      const input = EditableElement.getFormItemInputs()[0];

      const pageElement = EditableElement.getPageElementHandle();
      pageElement.addEventListener('input', () => {
        input.value = pageElement.textContent;
      });
      pageElement.focus();
    }

    replaceFormItemWithPageElement(EditableElement) {
      const pageElement = EditableElement.getPageElementHandle();
      if (pageElement) {
        pageElement.removeAttribute('contenteditable');
        pageElement.classList.remove('edit-plus-text-editing');
      }
    }

    getFocusedSelectors() {
      let selectors = super.getFocusedSelectors();
      selectors.push('.edit-plus-text-editing');
      return selectors;
    }

    applies(EditableElement) {
      return EditableElement.getHandleType() !== 'wrapper';
    }

    validationMessageHandle() {
      const EditableElement = Drupal.EditPlus.CurrentlyEditingElement;
      return EditableElement.getPageElementHandle();
    }

  }

  /**
   * Register the entity reference label field plugin.
   */
  window.addEventListener('EditPlusFieldManager.RegisterPlugins', e => {
    e.detail.manager.registerPlugin(new TextfieldPlugin());
  });

}
