/**
 * Field plugin base.
 *
 * A base class for field plugins.
 */
export class FieldPluginBase {

  constructor() {
    this.originalValue = null;
  }

  init() {}

  /**
   * Supported widgets.
   *
   * @type {[]}
   *   An array of widget ID's this plugin supports.
   */
  supportedWidgets = [];

  /**
   * Edit.
   *
   * @param EditableElement
   *   The element being edited.
   */
  edit(EditableElement) {
    Drupal.EditPlus.CurrentlyEditingElement = EditableElement;
    this.replacePageElementWithFormItem(EditableElement);
    this.focus(EditableElement);
    this.revealAncillary(EditableElement);
  }

  /**
   * Focus.
   *
   * Focus on the field after it has been clicked.
   *
   * @param EditableElement
   *   The element being edited.
   */
  focus(EditableElement) {
    // Prepare for the eventual de-focusing the form item. It would be easy to
    // add a formItem.onblur when the form item looses focus, but losing focus
    // on the form item doesn't necessarily mean we are done with it. e.g.
    // changing a text format in the sidebar. Let's keep track of what is
    // currently focused so that we can check if one of its ancillary elements
    // was clicked and remain focused.
    EditableElement.getFormItemWrapper().classList.add('edit-plus-focused');
    document.addEventListener('mousedown', this.watchFocus, true);

    this.focusElement(EditableElement)
  }

  /**
   * Focus element.
   *
   * This should be overridden by field plugins to provide field specific
   * instructions on how to give the element focus.
   *
   * @param EditableElement
   *   The element being edited.
   *
   * @param event
   */
  focusElement(EditableElement, event) {}

  /**
   * Watch focus.
   *
   * Once an element is being edited, monitor clicks to determine if the click
   * should cause the form item to lose focus. Field plugins can provide selectors
   * that do not make the form item loose focus when clicked. e.g. things in the
   * sidebar like field formatters etc.
   *
   * @param e
   *   The mousedown event.
   */
  watchFocus(e) {
    // Check that the clicked element is something that should cause the form
    // item to lose focus.
    const plugin = Drupal.EditPlus.CurrentlyEditingElement.plugin;
    const getCurrentlyFocusedSelectors = plugin.getFocusedSelectors();
    let remainFocused = false;
    for (let i = 0; i < getCurrentlyFocusedSelectors.length; i++) {
      const ignoredClick = e.target.closest(getCurrentlyFocusedSelectors[i]);
      if (ignoredClick !== null) {
        remainFocused = true;
        break;
      }
    }
    if (!remainFocused) {
      plugin.blur(e).catch(e => {
        // Don't blur if the field is invalid.
        if (e !== 'HTML validation failed') {
          console.error(e);
        }
      });
    }
  }

  /**
   * Blur.
   *
   * Remove focus from the focused form item.
   *
   * @param e
   *   The event, if any.
   */
  async blur(e = null) {
    const EditableElement = Drupal.EditPlus.CurrentlyEditingElement;

    if (!this.isValid(e)) {
      return Promise.reject('HTML validation failed');
    }

    // Blur the form item.
    document.removeEventListener('mousedown', this.watchFocus, true);
    if (Drupal.EditPlus.CurrentlyEditingElement.sizedPlaceholderHeightObserver) {
      Drupal.EditPlus.CurrentlyEditingElement.sizedPlaceholderHeightObserver.disconnect();
    }
    Drupal.EditPlus.CurrentlyEditingElement = null;
    this.hideAncillary(EditableElement);
    document.querySelectorAll('.edit-plus-focused').forEach(element => {
      element.classList.remove('edit-plus-focused');
    });
    return this.blurElement(EditableElement);
  }

  /**
   * Is valid.
   *
   * @param e
   *   The event if any. We prevent default because the mousedown event doesn't
   *   work well with validity checking.
   *
   * @returns {boolean}
   *   Whether the currently editing element passes HTML5 validation.
   */
  isValid(e = null) {
    const EditableElement = Drupal.EditPlus.CurrentlyEditingElement;
    // Run the HTML5 validation.
    const inputs = EditableElement.getFormItemInputs();
    if (inputs && inputs.length > 0) {
      if (e) {
        e.preventDefault();
      }
      let allValid = true;
      for (let input of inputs) {
        const valid = input.checkValidity();
        if (!valid) {
          // Display the validation message.
          const validationMessageHandle = this.validationMessageHandle() ?? input;
          const validationId = this.generateUniqueId(validationMessageHandle);
          let validationMessage = document.querySelector(`[data-edit-plus-validation-id="${validationId}"]`);
          if (!validationMessage) {
            // Add the validation message to the page.
            validationMessageHandle.insertAdjacentHTML('afterend', Drupal.theme('editPlusValidationMessage', input.validationMessage));
            validationMessage = validationMessageHandle.nextElementSibling;
            // Position the message.
            if (window.getComputedStyle(validationMessage.parentElement).position === 'static') {
              validationMessage.parentElement.dataset.originalPosition = 'static';
              validationMessage.parentElement.style.position = 'relative';
            }
            validationMessage.style.top = window.getComputedStyle(validationMessageHandle).height;
            // Relate the message to the input.
            validationMessageHandle.dataset.editPlusValidationId = validationMessage.dataset.editPlusValidationId = validationId;
            validationMessageHandle.insertAdjacentElement('afterend', validationMessage);

            this.clearValidationMessageOnUserInput(validationMessageHandle);
          }
        }
        if (allValid) {
          allValid = valid;
        }
      }
      if (!allValid) {
        return false;
      }
    }
    return true;
  }

  /**
   * Clear validation message on user input.
   *
   * @param validationMessageHandle
   *   The validation message handle.
   *   @see this.validationMessageHandle
   */
  clearValidationMessageOnUserInput (validationMessageHandle) {
    const removeValidationMessage = this.removeValidationMessage;
    // Listen for user input.
    validationMessageHandle.addEventListener('input', e => {
      removeValidationMessage(e.target);
    });
    // Listen for programmatic changes.
    const originalDescriptor = Object.getOwnPropertyDescriptor(HTMLInputElement.prototype, 'value');
    Object.defineProperty(validationMessageHandle, 'value', {
      get() {
        return originalDescriptor.get.call(this);
      },
      set(newValue) {
        originalDescriptor.set.call(this, newValue);
        removeValidationMessage(this);
      },
    });
  }

  /**
   * Remove validation message.
   *
   * @param inputElement
   *   The invalid input element with the validation message.
   */
  removeValidationMessage(inputElement) {
    if (inputElement.dataset.editPlusValidationId) {
      document.querySelector(`.edit-plus-validation-message[data-edit-plus-validation-id="${inputElement.dataset.editPlusValidationId}"]`).remove();
      if (inputElement.dataset.originPosition === 'static') {
        inputElement.parentElement.style.position = '';
      }
      delete inputElement.dataset.editPlusValidationId;
    }
  }

  /**
   * Generate unique ID.
   *
   * @param element
   *   The element that needs a unique ID.
   *
   * @returns {*|string}
   *   A unique ID based on the elements position in the DOM.
   */
  generateUniqueId(element) {
    if (element.id) {
      return element.id;
    }

    const path = [];
    let currentElement = element;

    while (currentElement && currentElement.nodeType === Node.ELEMENT_NODE) {
      let selector = currentElement.tagName.toLowerCase();

      // Add class or nth-child for uniqueness
      if (currentElement.className) {
        selector += `.${Array.from(currentElement.classList).join('.')}`;
      } else {
        const siblings = Array.from(currentElement.parentNode?.children || []);
        const index = siblings.indexOf(currentElement) + 1;
        selector += `:nth-child(${index})`;
      }

      path.unshift(selector);
      currentElement = currentElement.parentElement;
    }

    return path.join(' > ');
  }

  /**
   * Validation message handle.
   *
   * Most of the time we want to show the HTML5 validation messages on the form
   * item, but some fields like simple text fields use contenteditable, which
   * isn't supported by HTML5 validation. Let's perform the validation on the
   * form item, but show the message on the contenteditable element.
   *
   * @returns {Element|null}
   *   The element to show the validation messages on. Most of the time this
   *   should be null.
   */
  validationMessageHandle() {
    return null;
  }

  /**
   * Blur element.
   *
   * This should be overridden by field plugins to provide field specific
   * instructions on how to remove focus from the element. Since detecting if
   * the field has changes occurs at this point, after editing, it's recommended
   * for field plugins to call this.replaceFormItemWithPageElement(EditableElement)
   * when appropriate.
   *
   * @param EditableElement
   *   The element being edited.
   *
   * @returns {Promise<void>}
   *   Getting the form inputs needs to be async since we may need to wait on
   *   a CKEditor to be attached before getting changes.
   */
  blurElement(EditableElement) {
    return Promise.resolve();
  }

  /**
   * Reveal ancillary.
   *
   * Reveal things in the sidebar that pertain to this field e.g. formatter
   *
   * @param EditableElement
   *   The element being edited.
   */
  revealAncillary(EditableElement) {}

  /**
   * Hide ancillary.
   *
   * Reveal things in the sidebar that pertain to this field e.g. formatter
   *
   * @param EditableElement
   *   The current form item.
   */
  hideAncillary(EditableElement) {
    EditableElement.getFormItemWrapper().classList.add('edit-plus-hidden');
  }

  /**
   * Replace page element with form item.
   *
   * Visually moves the hidden form item from the form in the sidebar over the
   * editable element that was clicked in the main page area.
   *
   * @param EditableElement
   *   The element being edited.
   */
  replacePageElementWithFormItem(EditableElement) {
    const pageElement = EditableElement.getPageElementHandle();
    const formItem = EditableElement.getFormItemHandle();

    // Add a placeholder element that the form item will hover over.
    const sizedPlaceholderId = EditableElement.getSizedPlaceholderId()
    const sizedPlaceholder = document.createElement('div');
    sizedPlaceholder.id = sizedPlaceholderId;
    sizedPlaceholder.classList.add('sized-placeholder');
    pageElement.parentElement.insertBefore(sizedPlaceholder, pageElement);

    this.positionFormItemOverPageElement();
    document.addEventListener('scroll', this.positionFormItemOverPageElement);
    window.addEventListener('resize', this.positionFormItemOverPageElement);

    // Make the sized placeholder the same height as the form item.
    sizedPlaceholder.style.height = formItem.getBoundingClientRect().height + 'px';

    // Ensure the sizedPlaceholder height stays in sync with the formItem height.
    // As items are added or removed from the form item via ajax, the sizedPlaceholder
    // should grow or shrink accordingly.
    Drupal.EditPlus.CurrentlyEditingElement.sizedPlaceholderHeightObserver = new ResizeObserver(entries => {
      const sizedPlaceholder = document.querySelector('#' + sizedPlaceholderId);
      if (sizedPlaceholder) {
        for (let entry of entries) {
          sizedPlaceholder.style.height = entry.contentRect.height + 'px';
        }
      }
    });
    Drupal.EditPlus.CurrentlyEditingElement.sizedPlaceholderHeightObserver.observe(formItem);

    pageElement.classList.add('edit-plus-hidden');
    formItem.classList.add('edit-plus-editing');
    formItem.classList.remove('edit-plus-hidden');
  }

  /**
   * Replace form item with page element.
   *
   * Moves a form item back to the form in the sidebar from the main page area.
   *
   * @param EditableElement
   *   The element being edited.
   */
  replaceFormItemWithPageElement(EditableElement) {
    document.removeEventListener('scroll', this.positionFormItemOverPageElement);
    window.removeEventListener('resize', this.positionFormItemOverPageElement);

    const pageElement = EditableElement.getPageElementHandle();
    const formItem = EditableElement.getFormItemHandle();

    formItem.removeAttribute('style');
    formItem.classList.remove('edit-plus-editing');
    const sizedPlaceholder = document.querySelector('#' + EditableElement.getSizedPlaceholderId());
    if (sizedPlaceholder) {
      sizedPlaceholder.remove();
    }

    formItem.classList.add('edit-plus-hidden');
    pageElement.classList.remove('edit-plus-hidden');
  }

  /**
   * Position form item over page element.
   *
   * The form item is position: fixed over the placeholder. The
   * placeholder not only pushes content further down the page as if the
   * editable element is in place there, but it also gives us the dimensions that
   * the editable element should have.
   */
  positionFormItemOverPageElement() {
    const EditableElement = Drupal.EditPlus.CurrentlyEditingElement;
    const formItem = EditableElement.getFormItemHandle();
    const popOutWidth = EditableElement.getFormItemWrapper().dataset.popOutWidth ?? 200;
    const rectangle = document.querySelector('#' + EditableElement.getSizedPlaceholderId()).getBoundingClientRect();

    formItem.style.position = 'fixed';

    if (popOutWidth < rectangle.width) {
      // The form item will fit inline.
      formItem.style.left = rectangle.left + 'px';
      formItem.style.top = rectangle.top + 'px';
      formItem.style.width = rectangle.width + 'px';
      formItem.classList.remove('form-item-popout');
    } else {
      // The form item won't fit in there, make a pop out.
      formItem.style.top = rectangle.top + 'px';
      const width = (parseInt(popOutWidth) + 20);
      // Center the popup over the sizedPlaceholder.
      let left = (rectangle.left + (rectangle.width / 2)) - (popOutWidth / 2);
      // Ensure we aren't hanging over the left edge.
      const leftEdge = Drupal.displace.calculateOffset('left');
      left = left > leftEdge ? left : leftEdge;
      // Ensure we aren't hanging over the right edge.
      const rightEdge = document.documentElement.clientWidth - Drupal.displace.calculateOffset('right');
      left = (left + width) < rightEdge ? left : left - ((left + width) - rightEdge);

      formItem.style.left = left + 'px';
      formItem.style.width = width + 'px';
      formItem.classList.add('form-item-popout');
    }
  }

  /**
   * Get remain focused selectors.
   *
   * When an input like a text area or an inline editor has focus, we don't always
   * want to hide the editor when the user clicks on something else on the page.
   * e.g. clicking a Bold icon in the CKEditor or changing a text format in the
   * sidebar.
   *
   * @returns {string[]}
   *   An array of selectors that will not cause the current form item to lose
   *   focus if clicked.
   */
  getFocusedSelectors() {
    return [
      '.edit-plus-focused',
      '#navigation-plus-right-sidebar',
      '.ck-body-wrapper',
    ];
  }

  /**
   * Applies.
   *
   * When the plugin manager is selecting plugins, ensure that this plugin applies.
   *
   * @param EditableElement
   *   The editable element.
   * @returns {boolean}
   *   Whether this plugin should be used.
   */
  applies(EditableElement) {
    return true;
  }

}

