import * as entityForm from '../entity-form.js';
import * as fieldPluginBase from './field-plugin-base.js';

export default function ($ = jQuery, Drupal, once, dropZones) {

  /**
   * Media field plugin.
   */
  class MediaPlugin extends fieldPluginBase.FieldPluginBase {
    constructor() {
      super();
      this.originalValue = null;
    }
    supportedWidgets = [
      'media_library_widget',
    ];
    focusElement(EditableElement) {
      this.originalValue = EditableElement.getFormItemWrapper().querySelector('.edit-plus-field-value img')?.src ?? null;
    }
    blurElement(EditableElement) {
      const formItemWrapper = EditableElement.getFormItemWrapper();
      const newValue = formItemWrapper.querySelector('.edit-plus-field-value img')?.src ?? null;

      this.replaceFormItemWithPageElement(EditableElement);
      if (newValue && this.originalValue !== newValue) {
        entityForm.updateTempstore(EditableElement, newValue.length === 0);
      }
      return Promise.resolve();
    }

    getFocusedSelectors() {
      let selectors = super.getFocusedSelectors();
      selectors.push('.field--widget-media-library-widget', '#drupal-modal', '.ui-dialog-buttonpane');
      return selectors;
    }
  }

  /**
   * Register the default field plugin.
   */
  window.addEventListener('EditPlusFieldManager.RegisterPlugins', e => {
    e.detail.manager.registerPlugin(new MediaPlugin());
  });

}

