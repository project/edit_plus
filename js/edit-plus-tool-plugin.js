
(($, Drupal, once, displace) => {

  /**
   * Edit+'s Navigation + plugin
   */
  class EditPlusToolPlugin extends Drupal.NavigationPlus.ToolPluginBase {
    id = 'edit_plus';
    enable = () => {
      super.enable();
      return Drupal.EditPlus.EnableEditMode();
    }
    disable = async () => {
      return Drupal.EditPlus.DisableEditMode();
    }
  }
  Drupal.NavigationPlus.ToolManager.registerPlugin(new EditPlusToolPlugin());

})(jQuery, Drupal, once, Drupal.displace);
