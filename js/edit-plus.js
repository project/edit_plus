import * as effects from './edit_plus/effects.js'; // Don't remove!
import * as entityForm from './edit_plus/entity-form.js';
import * as editableElement from './edit_plus/editable-element.js';
import * as pluginManager from './edit_plus/field-plugin-manager.js';

(($, Drupal, once, displace) => {

  Drupal.EditPlus = {};
  Drupal.EditPlus.EditMode = 'enabled';

  /**
   * Currently Editing Element.
   *
   * Remember the focused form item. Instead of calling replaceFormItemWithPageElement
   * onblur, lets track the focused form item so that clicking things in the
   * sidebar doesn't lose focus on the currently edited form item.
   */
  Drupal.EditPlus.CurrentlyEditingElement = null;

  /**
   * Editable Element clicked.
   *
   * @param e
   *   The click event.
   */
  Drupal.EditPlus.EditableElementClicked = (e) => {
    if (e.target.hasAttribute('contenteditable')) {
      // We are editing a textfield.
      // @see textfield.js
      return;
    }

    // Fields can be nested. Only register one mousedown.
    e.preventDefault();
    e.stopPropagation();

    // @todo Make the default of which form is loaded initially configurable and
    // @todo add a UI to switch between the referenced entities forms.
    // @todo Default to the first form, e.g. entity reference or media library
    // @todo not the referenced entity form or the media entity form.
    let fieldValue = e.target.closest('.edit-plus-field-value');
    while (fieldValue.parentNode.closest('.edit-plus-field-value')) {
      fieldValue = fieldValue.parentNode.closest('.edit-plus-field-value');
    }

    const EditableElement = new editableElement.EditableElement(fieldValue.dataset.editPlusId);

    // Prevent clicks on a form item that is currently being updated.
    if (EditableElement.getPageElementHandle().classList.contains('disabled-updating')) {
      return;
    }

    entityForm.getForm(EditableElement).then((response, status) => {
      displace();
      // Set the widget now that the form is loaded.
      EditableElement.setWidget();

      if (Drupal.EditPlus.CurrentlyEditingElement) {
        Drupal.EditPlus.CurrentlyEditingElement.plugin.blur(e).then((response, status) => {
          EditableElement.plugin.edit(EditableElement);
        }).catch(e => {
          if (e !== 'HTML validation failed') {
            console.error(e);
          }
        });
      } else {
        EditableElement.plugin.edit(EditableElement);
      }


    }).catch((error) => {
      console.error('An error occurred while trying to edit an element:', error);
    });
  }

  /**
   * Enable edit mode.
   */
  Drupal.EditPlus.EnableEditMode = () => {
    return new Promise((resolve, reject) => {
      once.remove('EditPlusDisabled', '.edit-plus-field-value', document);
      once('EditPlusEnabled', '.edit-plus-field-value', document).forEach(editableField => {
        editableField.addEventListener('mousedown', Drupal.EditPlus.EditableElementClicked);
      });
      Drupal.EditPlus.DisableInteractiveElements();
      resolve();
    });
  }

  /**
   * Disable edit mode
   */
  Drupal.EditPlus.DisableEditMode = async () => {
    await Drupal.EditPlus.notUpdating();
    return new Promise((resolve, reject) => {
      once.remove('EditPlusEnabled', '.edit-plus-field-value', document);
      once('EditPlusDisabled', '.edit-plus-field-value', document).forEach(editableField => {
        editableField.removeEventListener('mousedown', Drupal.EditPlus.EditableElementClicked);
      });
      document.querySelectorAll('.edit-plus-form.navigation-plus-sidebar').forEach(sidebar => {
        sidebar.classList.add('navigation-plus-hidden');
        sidebar.removeAttribute('data-offset-right');
        displace();
      });

      Drupal.EditPlus.EnableInteractiveElements();
      resolve();
    });
  }

  /**
   * Not updating.
   *
   * Wait till edit+ is done updating.
   *
   * @returns {Promise<unknown>}
   */
  Drupal.EditPlus.notUpdating = () => {
    return new Promise(resolve => {
      const intervalId = setInterval(() => {
        if (!entityForm.editPlusIsUpdating) {
          clearInterval(intervalId);
          resolve();
        }
      }, 100);
    });
  }

  /**
   * Edit + persist state.
   *
   * Navigation + dispatches events for when Edit mode should be enabled, but we
   * need to persist the state through AJAX calls.
   */
  Drupal.behaviors.EditPlusPersistState = {
    attach: (context, settings) => {
      once('EditPlusPersistState', '.navigation-plus-entity-wrapper', context).forEach(element => {
        // Persist the state.
        if (
          sessionStorage.getItem('toolbarState') === 'open' &&
          localStorage.getItem('navigation_plus.tool.active') === 'edit_plus'
        ) {
          Drupal.EditPlus.EnableEditMode();
        }
      });
    }
  };

  // Register field plugins.
  pluginManager.default($, Drupal, once);

  /**
   * Disables interactive elements while in edit mode.
   */
  Drupal.EditPlus.DoNothing = (e) => {
    e.preventDefault();
    e.stopPropagation();
  };

  Drupal.EditPlus.DisableInteractiveElements = () => {
    $('.navigation-plus-entity-wrapper')
      .find('a')
      .each((index, link) => {
        // Disable clicking links while in edit mode.
        $(link).on('click touchstart', Drupal.EditPlus.DoNothing);
      });
  };

  Drupal.EditPlus.EnableInteractiveElements = () => {
    $('.navigation-plus-entity-wrapper')
      .find('a')
      .each((index, link) => {
        // Enable clicking links when leaving edit mode.
        $(link).off('click touchstart', Drupal.EditPlus.DoNothing);
      });
  };

  /**
   * An HTML5 validation message.
   *
   * @param {string} [message]
   *   (optional) The message shown on the UI.
   * @return {string}
   *   The HTML markup for the validation message.
   */
  Drupal.theme.editPlusValidationMessage = (message) => {
    return `<div class="edit-plus-validation-message">${message}</div>`;
  };

})(jQuery, Drupal, once, Drupal.displace);




