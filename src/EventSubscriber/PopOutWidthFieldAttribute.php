<?php

declare(strict_types=1);

namespace Drupal\edit_plus\EventSubscriber;

use Drupal\edit_plus\Event\FieldAttributes;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Pop out width field attribute.
 */
class PopOutWidthFieldAttribute implements EventSubscriberInterface {

  public function addFormItemAttributesAlter(FieldAttributes $event) {
    $form = $event->getForm();
    $field_name = $event->getFieldName();
    $form_item = &$form[$field_name];
    if (empty($form_item['#attributes']['data-pop-out-width'])) {
      $form_item['#attributes']['data-pop-out-width'] = '200';
    }

    $event->setForm($form);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      FieldAttributes::ALTER => ['addFormItemAttributesAlter'],
    ];
  }

}
