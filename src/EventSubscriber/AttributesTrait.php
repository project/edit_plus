<?php

declare(strict_types=1);

namespace Drupal\edit_plus\EventSubscriber;

use Drupal\edit_plus\Event\FieldAttributes;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;

trait AttributesTrait {

  protected static function getWidget(FieldAttributes $event): ?string {
    $field_name = $event->getFieldName();
    $form_display = $event->getFormState()->get('form_display');
    if (empty($form_display))  {
      // Must not be an entity form, load the form display.
      $entity = $event->getEntity();
      $form_display = static::getEntityDisplayRepository()->getFormDisplay($entity->getEntityTypeId(), $entity->bundle());
    }

    return $form_display->getComponent($field_name)['type'];
  }

  /**
   * Get entity display repository.
   *
   * @return \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   *   The entity display repository service.
   */
  protected static function getEntityDisplayRepository(): EntityDisplayRepositoryInterface {
    return \Drupal::service('entity_display.repository');
  }

}
