<?php

declare(strict_types=1);

namespace Drupal\edit_plus\EventSubscriber;

use Drupal\Core\Render\Element;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\edit_plus\Event\FieldAttributes;
use Drupal\edit_plus\Event\FieldProperties;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Default field attributes.
 */
class DefaultFieldAttributes implements EventSubscriberInterface {

  use AttributesTrait;

  public function __construct(
    protected EventDispatcherInterface $eventDispatcher,
  ) {}

  /**
   * Add form item attributes.
   *
   * Add attributes that associate the form element with the rendered
   * page markup.
   *
   * @param \Drupal\edit_plus\Event\FieldAttributes $event
   *   The event.
   */
  public function addFormItemAttributesAlter(FieldAttributes $event) {
    $field_name = $event->getFieldName();
    $form = $event->getForm();
    $form_item = &$form[$field_name];
    $entity = $event->getEntity();
    $form_item['#attributes']['data-edit-plus-form-item-wrapper-id'] = sprintf('%s::%s::%s::%s', $entity->getEntityTypeId(), edit_plus_entity_identifier($entity), $field_name, 'value');
    $form_item['#attributes']['data-edit-plus-form-item-widget'] = self::getWidget($event);
    $form_item['#attributes']['class'][] = 'edit-plus-form-item-wrapper';

    $display = EntityViewDisplay::collectRenderDisplay($entity, $event->getViewMode())->toArray();
    if (empty($display['hidden'][$field_name])) {
      // Hide form items of elements that will be edited inline.
      $form_item['#attributes']['class'][] = 'edit-plus-hidden';
    } else {
      // If the field is hidden in the view mode, it can't be edited inline.
      $form_item['#attributes']['data-edit-plus-auto-submit'] = '';
    }

    $event->setForm($form);
  }

  public function addFormItemAttributesAfterBuild(FieldAttributes $event) {
    $form_state = $event->getFormState();
    $form = $event->getForm();
    $entity = $event->getEntity();
    $field_name = $event->getFieldName();
    $form_item = &$form[$field_name];

    $properties = $entity->getFieldDefinition($field_name)->getFieldStorageDefinition()->getPropertyNames();
    // Allow modules to specify custom properties.
    $properties = $this->eventDispatcher->dispatch(new FieldProperties($entity, $field_name, $properties), FieldProperties::class)->getProperties();

    if (!empty($form_item['widget']['#theme']) && $form_item['widget']['#theme'] === 'field_multiple_value_form') {
      foreach (Element::children($form_item['widget']) as $delta) {
        foreach ($properties as $property) {
          if (!empty($form_item['widget'][$delta][$property])) {
            $form_item['widget'][$delta][$property] = $this->addFormItemAttributes($form_state, $form_item['widget'][$delta][$property], $entity, $field_name, $delta, $property);
          }
        }
      }
    }
    else if (!empty($form_item['widget']['#type']) && $form_item['widget']['#type'] === 'checkboxes') {
      foreach (Element::children($form_item['widget']) as $delta) {
        foreach ($properties as $property) {
          if (!empty($form_item['widget'][$delta])) {
            $form_item['widget'][$delta] = $this->addFormItemAttributes($form_state, $form_item['widget'][$delta], $entity, $field_name, $delta, $property);
          }
        }
      }

    }
    // Add attributes to fields with no children.
    else {
      $form_item['widget'] = $this->addFormItemAttributes($form_state, $form_item['widget'], $entity, $field_name, '0', end($properties));
    }
    $event->setForm($form);
  }

  // Add attributes that associate the form element with the rendered
  // page markup.
  private function addFormItemAttributes(FormStateInterface $form_state, &$form_item, EntityInterface $entity, $field_name, $delta, $property) {
    $form_item_id = sprintf('%s::%s::%s::%s::%s', $entity->getEntityTypeId(), edit_plus_entity_identifier($entity), $field_name, $delta, $property);

    $form_item['#wrapper_attributes'] = new Attribute([
      'class' => ['edit-plus-form-item'],
      'data-edit-plus-form-item-id' => $form_item_id,
    ]);
    if (!empty($form_item['#format'])) {
      $form_item['#wrapper_attributes']->setAttribute('data-edit-plus-format', $form_item['#format']);
    }
    // Add an effect to fields that were just added to the page.
    if ($form_state->get('added_to_page') === $field_name) {
      $definition = $entity->getFieldDefinition($field_name);
      $main_property = method_exists($definition, 'getMainPropertyName') ? $definition->getMainPropertyName() : 'value';
      if ($main_property === $property) {
        $form_item['#wrapper_attributes']->addClass('edit-plus-field-added-to-page');
      }
    }
    return $form_item;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      FieldAttributes::ALTER => ['addFormItemAttributesAlter', -1],
      FieldAttributes::AFTER_BUILD => ['addFormItemAttributesAfterBuild', -1],
    ];
  }

}
