<?php

declare(strict_types=1);

namespace Drupal\edit_plus\EventSubscriber;

use Drupal\edit_plus\Event\FieldAttributes;
use Drupal\Core\Field\FieldDefinitionInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Handle field attribute.
 *
 * The "handle" is what DOM element will be used when swapping the form item
 * into the markup of the page for editing. Options:
 *  - form_item:
 */
class HandleFieldAttribute implements EventSubscriberInterface {

  public function addFormItemAttributesAlter(FieldAttributes $event) {
    $form = $event->getForm();
    $entity = $event->getEntity();
    $field_name = $event->getFieldName();
    $field_definition = $entity->getFieldDefinition($field_name);
    $handle = self::getDefault($field_definition);
    if (method_exists($field_definition, 'getThirdPartySettings')) {
      $third_party_settings = $field_definition->getThirdPartySettings('edit_plus');
      if (!empty($third_party_settings['handle'])) {
        $handle = $third_party_settings['handle'];
      }
    }
    $form[$field_name]['#attributes']['data-edit-plus-handle'] = $handle;
    $event->setForm($form);
  }

  public static function getDefault(FieldDefinitionInterface $field_definition) {
    $type = $field_definition->getType();
    $map = [
      'entity_reference' => 'wrapper',
      'entity_reference_revisions' => 'wrapper',
    ];
    if (!empty($map[$type])) {
      return $map[$type];
    } elseif ($field_definition->getFieldStorageDefinition()->getCardinality() !== 1) {
      return 'wrapper';
    } else {
      return 'form_item';
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      FieldAttributes::ALTER => ['addFormItemAttributesAlter'],
    ];
  }

}
