<?php

namespace Drupal\edit_plus\Drush\Commands;

use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Drupal\Component\Serialization\Json;

/**
 * Update CKEditor Inline Editor.
 */
final class UpdateInlineEditor extends DrushCommands {

  /**
   * Update version.
   */
  #[CLI\Command(name: 'edit_plus:update-ckeditor-version', aliases: ['e+_update'])]
  #[CLI\Usage(name: 'e+_update', description: 'Extracts the ckeditor version that core is currently using and updates Edit+\'s package.json.')]
  public function updateVersion() {
    $cores_packages = Json::decode(file_get_contents(DRUPAL_ROOT . '/core/package.json'));
    $ckeditor5_version = $cores_packages['devDependencies']['ckeditor5'];
    $edit_plus_path = \Drupal::service('extension.list.module')->getPath('edit_plus'); // @todo inject.
    $package_json_path = DRUPAL_ROOT . "/$edit_plus_path/package.json";
    $edit_plus_packages = Json::decode(file_get_contents($package_json_path));
    $edit_plus_packages['devDependencies']['@ckeditor/ckeditor5-editor-inline'] = $ckeditor5_version;
    file_put_contents($package_json_path, json_encode($edit_plus_packages, JSON_PRETTY_PRINT));
    $this->logger()->success(dt("Updated @ckeditor/ckeditor5-editor-inline in edit_plus/package.json to $ckeditor5_version."));
  }

  /**
   * Move inline editor library.
   */
  #[CLI\Command(name: 'edit_plus:move-library', aliases: ['e+_move'])]
  #[CLI\Usage(name: 'e+_update', description: 'Extracts the inline editor library from node_modules and moves it to assets/vendor.')]
  public function moveLibrary() {
    $edit_plus_path = \Drupal::service('extension.list.module')->getPath('edit_plus'); // @todo inject.
    $from_library_path = DRUPAL_ROOT . "/$edit_plus_path/node_modules/@ckeditor/ckeditor5-editor-inline/build/editor-inline.js";
    $to_library_path = DRUPAL_ROOT . "/$edit_plus_path/assets/vendor/ckeditor5/editor-inline.js";
    copy($from_library_path, $to_library_path);
    $this->logger()->success(dt("Moved @ckeditor/ckeditor5-editor-inline library to $to_library_path."));
  }

}
