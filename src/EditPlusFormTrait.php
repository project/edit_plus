<?php

namespace Drupal\edit_plus;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityInterface;
use Drupal\edit_plus\Ajax\UpdateMarkup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\edit_plus\Event\AddEmptyField;
use Drupal\edit_plus\Event\FieldAttributes;
use Drupal\edit_plus\Ajax\ShowFieldWithErrors;
use Drupal\edit_plus\EventSubscriber\HandleFieldAttribute;

trait EditPlusFormTrait {

  public function prepareFormForInlineEditing(&$form, FormStateInterface $form_state) {
    $entity = $this->getFormEntity($form, $form_state);
    $entity = $this->editPlusTempstoreRepository->get($entity);

    // Hide the form buttons.
    $form['actions']['#attributes']['class'][] = 'edit-plus-hidden';

    // Let the JS report that a field has been emptied.
    // @see entity-form.js updateTempstore()
    $form['empty_field'] = [
      '#type' => 'hidden',
      '#value' => NULL,
      '#attributes' => [
        'id' => 'empty-field',
      ],
    ];

    // Pass the view mode along to subsequent page requests.
    $form['view_mode'] = [
      '#type' => 'hidden',
      '#attributes' => ['class' => ['edit-plus-view-mode']],
      '#value' => $this->getViewMode($form, $form_state, $entity),
    ];

    // Let entity-form.js updateTempstore flag to only update the markup of the
    // changed editable element instead of the entire entity. This allows users
    // to continue editing other fields.
    $form['only_update_element'] = [
      '#type' => 'hidden',
      '#attributes' => ['class' => ['edit-plus-only-update-element']],
      '#value' => NULL,
    ];

    foreach ($entity->getFieldDefinitions() as $field_name => $field_definition) {
      if (!empty($form[$field_name])) {
        $this->addFieldToPageButton($form, $entity, $field_name);
      }
    }

    // Attribute form items so they are correlated with the rendered markup.
    $this->attributeFormItemsEventDispatcher($form, $form_state, FieldAttributes::ALTER);
    $form['#after_build'][] = [$this, 'prepareFormItemsForInlineEditingAfterBuild'];
  }

  public function prepareFormItemsForInlineEditingAfterBuild($form, FormStateInterface $form_state) {
    $this->attributeFormItemsEventDispatcher($form, $form_state, FieldAttributes::AFTER_BUILD);
    $form_state->set('added_to_page', NULL);
    return $form;
  }

  private function attributeFormItemsEventDispatcher(&$form, FormStateInterface $form_state, string $event_name) {
    $entity = $this->getFormEntity($form, $form_state);
    $entity = $this->editPlusTempstoreRepository->get($entity);
    $view_mode = $this->getViewMode($form, $form_state, $entity);
    // Add attributes to associate the form item with the rendered markup.
    foreach ($entity->getFieldDefinitions() as $field_name => $field_definition) {
      if (!empty($form[$field_name])) {
        $event = new FieldAttributes($form, $view_mode, $field_name, $entity, $form_state);
        $this->dispatcher->dispatch($event, $event_name);
        $form = $event->getForm();
      }
    }
    // Auto submit the items in the advanced area.
    if (!empty($form['advanced']['group']['#groups']['advanced'])) {
      foreach ($form['advanced']['group']['#groups']['advanced'] as $advanced_form_item) {
        if (is_array($advanced_form_item)) {
          foreach ($advanced_form_item as $key => $advanced_form_item_component) {
            if (!str_starts_with($key, '#') && !empty($advanced_form_item_component['#array_parents'])) {
              $form_item =& NestedArray::getValue($form, $advanced_form_item_component['#array_parents']);
              $form_item['#attributes']['data-edit-plus-auto-submit'] = '';
            }
          }
        }
      }
    }
  }

  /**
   * Add empty field to page button.
   *
   * Only fields with values are rendered on the canonical page, so we'd
   * have nothing to click to inline edit the field... Let's add a button in the
   * sidebar (entity form) that let's you add the field to the page with
   * auto-generated content.
   *
   * @param $form
   *   The form
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $form_item
   *   The field name/form item to possibly add a button for if it's empty.
   */
  public function addFieldToPageButton(&$form, EntityInterface $entity, string $form_item) {
    $field = $entity->get($form_item);
    $field_definition = $field->getFieldDefinition();
    if ($field->isEmpty() && method_exists($field_definition, 'label')) {
      // Empty field items get a button to add them to the page.
      $form['empty_fields']['#type'] = 'details';
      $form['empty_fields']['#open'] = TRUE;
      $form['empty_fields']['#title'] = t('Add field');
      $form['empty_fields'][$form_item] = [
        '#type' => 'submit',
        '#value' => t('@label', ['@label' => $field_definition->label()]),
        '#name' => "add_empty_field::$form_item",
        '#submit' => [
          [$this, 'populateEmptyField'],
        ],
        '#ajax' => [
          'callback' => [$this, 'addEmptyField'],
        ],
      ];
    }
  }

  /**
   * Add empty field submit handler.
   *
   * Generate field values before adding an empty field to the page.
   */
  public function populateEmptyField(&$form, FormStateInterface $form_state) {
    $entity = $this->editPlusTempstoreRepository->get($this->getFormEntity($form, $form_state));
    $input = $form_state->getUserInput();
    [$_, $field_name] = explode('::', $input['_triggering_element_name']);
    $field = $entity->get($field_name);

    // Generate a value for the field about to be placed on the page.
    if ($this->moduleHandler->moduleExists('field_sample_value')) {
      $this->fieldGenerator->populateWithSampleValue($field);
    } else {
      $field->generateSampleItems();
    }
    $this->dispatcher->dispatch(new AddEmptyField($field), AddEmptyField::class);
    $this->updateFormState($form, $form_state, $field, $entity);
    Cache::invalidateTags([getCacheTag($entity)]);

    // Flag that the newly added field needs an "added" effect
    $form_state->set('added_to_page', $field_name);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Ajax callback.
   *
   * Now that a field had its value set in populateEmptyField, update the
   * form and page.
   */
  public function addEmptyField(&$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $this->updatePage($response, $form, $form_state);
    $this->updateForm($response, $form, $form_state);
    $this->renderMessages($response);

    return $response;
  }

  /**
   * Update the rendered page.
   *
   * @param \Drupal\Core\Ajax\AjaxResponse $response
   *   The ajax response.
   * @param $form
   *   The entity form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function updatePage(AjaxResponse $response, array &$form, FormStateInterface $form_state) {
    $entity = $this->getFormEntity($form, $form_state);
    $view_mode = $this->getViewMode($form, $form_state, $entity);
    $content = $this->entityContent($form, $form_state, $view_mode);
    $input = $this->getUserInput($form_state);
    if (!empty($content['component']['content']['#view_mode']) && $content['component']['content']['#view_mode'] !== $view_mode) {
      // It's possible that the view mode was changed in this update. $view_mode
      // needs to remain as it was for replacing the old content on the page, but
      // let's update the view mode for passing it on down the line.
      if (!empty($form['settings']['block_form']['view_mode']['#value'])) {
        // @todo This only covers inline blocks I think since it looks for the new view mode in component > content > #view_mode.
        // Ensure that we don't need to cover other situations like entity forms.
        $form['settings']['block_form']['view_mode']['#value'] = $content['component']['content']['#view_mode'];
      }

      $input['view_mode'] = $content['component']['content']['#view_mode'];
      $form_state->setUserInput($input);
    }

    $selector = sprintf('[data-navigation-plus-entity-wrapper="%s::%s::%s"][data-navigation-plus-view-mode="%s"]', $entity->getEntityTypeId(), edit_plus_entity_identifier($entity), $entity->bundle(), $view_mode);
    $response->addCommand(new UpdateMarkup($selector, $content, $input['only_update_element']));
  }

  /**
   * Entity Content
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param string|NULL $view_mode
   *   The view mode.
   *
   * @return array
   *   A render array of the entity content to return as the updated page.
   */
  abstract public function entityContent(array &$form, FormStateInterface $form_state, string $view_mode = NULL);

  /**
   * Update the entity form.
   *
   * @param \Drupal\Core\Ajax\AjaxResponse $response
   *   The ajax response.
   * @param $form
   *   The entity form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function updateForm(AjaxResponse $response, &$form, FormStateInterface $form_state) {
    $entity = $this->getFormEntity($form, $form_state);
    $form_id = self::getEditPlusFormId($entity);
    $response->addCommand(new ReplaceCommand("#$form_id", $this->wrapForm($form, $entity)));
  }

  /**
   * Handle errors.
   *
   * @param \Drupal\Core\Ajax\AjaxResponse $response
   *   The ajax response.
   * @param $form
   *   The entity form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function handleErrors(AjaxResponse $response, &$form, FormStateInterface $form_state) {
    $errors = $form_state->getErrors();
    if (!empty($errors)) {
      // $form_state->getErrors() are keyed by #parents. $form['#children_errors']
      // are keyed by the array parents. Find the element of the $form_state->getErrors()
      // based on the #array_parents of the $form['#children_errors'].
      // There are too many child errors, so we should use $form_state->getErrors().
      foreach ($errors as $error_keyed_by_parents => $_) {
        foreach ($form['#children_errors'] as $error_keyed_by_array_parents => $_) {
          $array_parents = explode('][', $error_keyed_by_array_parents);
          $form_item = NestedArray::getValue($form, $array_parents);
          $parents_key = implode('][', $form_item['#parents']);
          if ($parents_key === $error_keyed_by_parents) {
            // Find the field for this error.
            $entity = $this->getFormEntity($form, $form_state);
            foreach ($array_parents as $field_name) {
              if ($entity->hasField($field_name)) {
                break;
              }
            }
            // Find the form item handle for this field.
            $field_definition = $entity->getFieldDefinition($field_name);
            $third_party_settings = $field_definition->getThirdPartySettings('edit_plus');
            $handle = $third_party_settings['handle'] ?? HandleFieldAttribute::getDefault($field_definition);

            // Build the form item handle css selector so it can be edited again
            // to show that it has an error.
            if ($handle === 'form_item') {
              if (!empty($form_item['#wrapper_attributes']) && $form_item['#wrapper_attributes']->hasAttribute('data-edit-plus-form-item-id')) {
                $form_item_id = $form_item['#wrapper_attributes']->storage()['data-edit-plus-form-item-id']->__toString();
                $selector = "[data-edit-plus-form-item-id='$form_item_id']";
              }
            } else {
              // Search for the wrapper.
              $array_parents_search = [];
              foreach ($array_parents as $key) {
                $array_parents_search[] = $key;
                $form_item = NestedArray::getValue($form, $array_parents_search);
                if (!empty($form_item['#attributes']['data-edit-plus-form-item-wrapper-id'])) {
                  $wrapper_id = $form_item['#attributes']['data-edit-plus-form-item-wrapper-id'];
                  $selector = "[data-edit-plus-form-item-wrapper-id='$wrapper_id']";
                  break;
                }
              }
            }

            // Flag that after the error is cleared the form needs to be updated again
            // in order to remove the red border.
            $form['previously_had_errors'] = [
              '#type' => 'hidden',
              '#name' => 'previously_had_errors',
              '#value' => 'true',
            ];
            // Update the form so invalid fields get bordered with red.
            $this->updateForm($response, $form, $form_state);

            // @todo Could there be more than one error at a time?
            // All errors are being rendered to the page in renderMessages()
            // which could be confusing to see multiple errors, but only see one
            // form element. But this works fine for now since with our current
            // workflow we only expect one error at a time, but ultimately I
            // think we should remove the display of all errors except for the
            // element that is going to be shown. When that error is resolved
            // then show the next one, and so on.
            // @todo $form_state->clearErrors();
            // @todo Add only this error.
            $response->addCommand(new ShowFieldWithErrors($selector, $handle));
            break 2;
          }
        }
      }
    }
  }

  /**
   * Get view display.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to get the display from.
   * @param string $view_mode
   *   The view mode for the display.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity view display for the given entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getViewDisplay(EntityInterface $entity, string $view_mode) {
    $display_id = $entity->getEntityTypeId() . '.' . $entity->bundle() . '.' . $view_mode;
    static $view_displays;
    if (!empty($view_displays[$display_id])) {
      return $view_displays[$display_id];
    }
    $view_display = $this->entityTypeManager->getStorage('entity_view_display')->load($display_id);
    if (empty($view_display)) {
      $view_display = $this->entityTypeManager->getStorage('entity_view_display')->load($entity->getEntityTypeId() . '.' . $entity->bundle() . '.' . 'default');
    }
    $view_displays[$display_id] = $view_display;
    return $view_display;
  }

  public static function getEditPlusFormId(EntityInterface $entity) {
    $id = edit_plus_entity_identifier($entity);
    return "edit-plus-form_{$entity->getEntityTypeId()}-{$id}";
  }

  /**
   * Wrap form.
   *
   * Wraps a form for easy identification in JS and ajax replacement. It's best
   * to wrap it after the form has been built and processed.
   *
   * @param $form
   *   The entity form to wrap.
   * @param $entity
   *   The entity.
   *
   * @return array
   *   An entity form wrapped in a container.
   */
  public function wrapForm($form, $entity) {
    return [
        // Add an AJAX wrapper.
        '#type' => 'container',
        '#tree' => FALSE,
        '#attributes' => [
          'id' => self::getEditPlusFormId($entity),
          'class' => ['edit-plus-form', 'navigation-plus-sidebar', 'right-sidebar'],
          'data-edit-plus-form-id' => sprintf('%s::%s', $entity->getEntityTypeId(), edit_plus_entity_identifier($entity)),
          'data-offset-right' => '',
        ],
        'form' => $form,
      ];
  }

  public function renderMessages(AjaxResponse $response) {
    $status_messages = ['#type' => 'status_messages'];
    $messages = $this->renderer->renderRoot($status_messages);
    $response->addCommand(new RemoveCommand('[data-drupal-messages]'));
    if (!empty($messages)) {
      $response->addCommand(new InvokeCommand('[data-drupal-messages-fallback]', 'after', [$messages]));
    }
  }

  /**
   * Get view mode.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *    The form state.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return string
   *   The view mode of the entity being edited.
   */
  public function getViewMode(array &$form, FormStateInterface $form_state, EntityInterface $entity) {
    // View mode comes from the initial JS that loads the form or from the previously
    // loaded form for subsequent form loads. e.g. Adding an empty field to the page.
    $view_mode = $this->request->get('viewMode');
    if (empty($view_mode)) {
      // When the form is submitted we are going to need to update the rendered page
      // so we need the view mode. Make the form aware of this value so we can set
      // it with JS before submitting.
      $input = $form_state->getUserInput();
      if (!empty($input['settings']['block_form']['view_mode'])) {
        // Block content entity form.
        $view_mode = $input['settings']['block_form']['view_mode'];
      }
      elseif (!empty($input['view_mode'])) {
        // Entity form.
        $view_mode = $input['view_mode'];
      }
    }
    // Sanitize view mode.
    $valid_view_modes = $this->entityDisplayRepository->getViewModes($entity->getEntityTypeId());
    $valid_view_modes['default'] = TRUE;
    if (empty($valid_view_modes[$view_mode])) {
      throw new \InvalidArgumentException((string) t('Edit +: Invalid view mode for @label', ['@label' => $entity->label()]));
    }

    return $view_mode;
  }

}

