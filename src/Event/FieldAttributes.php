<?php

namespace Drupal\edit_plus\Event;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\EventDispatcher\Event;

class FieldAttributes extends Event {

  const ALTER = 'field_attributes.alter';
  const AFTER_BUILD = 'field_attributes.after_build';

  public function __construct(
    private array $form,
    private string $view_mode,
    private string $field_name,
    private EntityInterface $entity,
    private FormStateInterface $form_state,
  ) {}

  /**
   * @return \Drupal\Core\Form\FormStateInterface
   */
  public function getFormState(): FormStateInterface {
    return $this->form_state;
  }

  /**
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function setFormState(FormStateInterface $form_state): void {
    $this->form_state = $form_state;
  }

  /**
   * @return mixed
   */
  public function getForm() {
    return $this->form;
  }

  /**
   * @param mixed $form
   */
  public function setForm($form): void {
    $this->form = $form;
  }

  /**
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * @return mixed
   */
  public function getFieldName() {
    return $this->field_name;
  }

  /**
   * @return mixed
   */
  public function getDelta() {
    return $this->delta;
  }

  /**
   * @return mixed
   */
  public function getProperty() {
    return $this->property;
  }

  /**
   * @return mixed
   */
  public function getViewMode() {
    return $this->view_mode;
  }

}
