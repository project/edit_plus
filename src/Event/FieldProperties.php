<?php

namespace Drupal\edit_plus\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;

class FieldProperties extends Event {

  /**
   * An array of field properties.
   */
  private array $properties;

  /**
   * @var \Drupal\Core\Entity\EntityInterface
   */
  private EntityInterface $entity;

  /**
   * The field name.
   */
  private string $fieldName;

  public function __construct(EntityInterface $entity, string $field_name, array $properties) {
    $this->properties = $properties;
    $this->entity = $entity;
    $this->fieldName = $field_name;
  }

  /**
   * Get properties.
   *
   * @return
   *   An array of properties on this field.
   */
  public function getProperties() {
    return $this->properties;
  }

  /**
   * @param array $properties
   *   An array of field properties.
   */
  public function setProperties(array $properties) {
    $this->properties = $properties;
  }

  /**
   * Get entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity that this field is on.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * Get field name.
   *
   * @return string
   *   The field name.
   */
  public function getFieldName(): string {
    return $this->fieldName;
  }

}
