<?php

namespace Drupal\edit_plus\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Field\FieldItemListInterface;

class AddEmptyField extends Event {

  public function __construct(
    private FieldItemListInterface $field
  ) {}

  /**
   * Get field.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface
   *   The field that is having it's value generated and about to be placed on
   *   the page.
   */
  public function getField(): FieldItemListInterface {
    return $this->field;
  }

  /**
   * Set field.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   The field that is having it's value generated and about to be placed on
   *   the page.
   */
  public function setField(FieldItemListInterface $field): void {
    $this->field = $field;
  }

}
