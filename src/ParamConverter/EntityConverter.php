<?php

namespace Drupal\edit_plus\ParamConverter;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\edit_plus\EditPlusTempstoreRepository;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\ParamConverter\EntityConverter as EntityConverterBase;

/**
 * Entity Converter.
 *
 * Uses the Edit + tempstore version of an entity if it exist.
 */
class EntityConverter extends EntityConverterBase {

  use StringTranslationTrait;

  public function __construct(
    protected ParamConverterInterface $inner,
    EntityTypeManagerInterface $entity_type_manager,
    EntityRepositoryInterface $entity_repository,
    protected EditPlusTempstoreRepository $editPlusTempstoreRepository,
    protected MessengerInterface $messenger
  ) {
    parent::__construct($entity_type_manager, $entity_repository);
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    $entity = parent::convert($value, $definition, $name, $defaults);
    if (!\Drupal::currentUser()->hasPermission('access inline editing')) {
      return $entity;
    }

    return $this->loadEntityFromTempstore($entity);
  }

  /**
   * Load entity from tempstore.
   *
   * @param $entity
   *
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The tempstore version of the entity if it exists or the original entity.
   */
  public function loadEntityFromTempstore($entity) {
    // Only use the tempstore when in Edit mode.
    $mode = \Drupal::service('navigation_plus.ui')->getMode();
    if ($mode !== 'edit') {
      return $entity;
    }
    // Use the Edit + tempstore version of an entity if it exist.
    $tempstore_entity = $this->editPlusTempstoreRepository->get($entity);
    if ($tempstore_entity) {
      $tempstore_entity_array = $tempstore_entity->toArray();
      $entity_array = $entity->toArray();
      $difference = $this->recursiveArrayDiffAssoc($tempstore_entity_array, $entity_array);
      if (!empty($difference)) {
        $entity = $tempstore_entity;
        // Flag that we have swapped this entity with a tempstore version.
        edit_plus_active_tempstore_entities($entity);
      }
    }
    return $entity;
  }

  private function recursiveArrayDiffAssoc($array1, $array2) {
    $difference = [];

    foreach ($array1 as $key => $value) {
      if (is_array($value) && isset($array2[$key]) && is_array($array2[$key])) {
        $diff = $this->recursiveArrayDiffAssoc($value, $array2[$key]);
        if (!empty($diff)) {
          $difference[$key] = $diff;
        }
      } elseif (!array_key_exists($key, $array2) || $value !== $array2[$key]) {
        $difference[$key] = $value;
      }
    }

    return $difference;
  }

  // @todo This doesn't account for when you are in a workspace and just start editing a node.
  // @todo The tempstore is going, but workspaces hasn't started tracking the entity.
  // @todo We are going to prevent editing the same node in multiple workspaces for now,
  // @todo but when we do enable it the tempstore should just be prefixed with the
  // @todo workspace ID.
  //  /**
  //   * {@inheritdoc}
  //   */
  //  public function convert($value, $definition, $name, array $defaults) {
  //    $entity = parent::convert($value, $definition, $name, $defaults);
  //    if (!\Drupal::currentUser()->hasPermission('access inline editing')) {
  //      return $entity;
  //    }
  //
  //    if (\Drupal::moduleHandler()->moduleExists('workspaces')) {
  //      // Ensure we are in the right workspace.
  //      $workspace_manager = \Drupal::service('workspaces.manager');
  //      if ($workspace_manager->hasActiveWorkspace()) {
  //        $workspace = $workspace_manager->getActiveWorkspace();
  //        $tracked_entities = \Drupal::service('workspaces.association')->getTrackedEntities($workspace->id());
  //        if (!empty($tracked_entities[$name])) {
  //          $tracked_entities[$name] = array_flip($tracked_entities[$name]);
  //          if (!empty($tracked_entities[$name][$value])) {
  //            return $this->loadEntityFromTempstore($entity);
  //          }
  //        }
  //      }
  //    } else {
  //      return $this->loadEntityFromTempstore($entity);
  //    }
  //
  //
  //    return $entity;
  //  }

}
