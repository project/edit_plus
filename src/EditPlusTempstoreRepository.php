<?php

namespace Drupal\edit_plus;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\TempStore\SharedTempStoreFactory;

/**
 * Provides an Edit + tempstore.
 */
class EditPlusTempstoreRepository {

  public function __construct(
    protected SharedTempStoreFactory $tempStoreFactory
  ) {}

  /**
   * Get the tempstore version of an entity, if it exists.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The tempstore version of the entity.
   */
  public function get(EntityInterface $entity) {
    $key = $this->getKey($entity);
    $tempstore = $this->getTempstore($entity)->get($key);
    if (!empty($tempstore['entity'])) {
      $entity = $tempstore['entity'];

      if (!($entity instanceof EntityInterface)) {
        throw new \UnexpectedValueException(sprintf('The "%s" entry is invalid', $key));
      }
    }
    return $entity;
  }

  /**
   * Checks for the existence of a tempstore version of the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *  The entity.
   *
   * @return bool
   *   TRUE if there is a tempstore version of this entity.
   */
  public function has(EntityInterface $entity) {
    $key = $this->getKey($entity);
    $tempstore = $this->getTempstore($entity)->get($key);
    return !empty($tempstore['entity']);
  }

  /**
   * Store this entity in the tempstore.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity
   */
  public function set(EntityInterface $entity) {
    $key = $this->getKey($entity);
    $this->getTempstore($entity)->set($key, ['entity' => $entity]);
  }

  /**
   * Removes the tempstore version of the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  public function delete(EntityInterface $entity) {
    $key = $this->getKey($entity);
    $this->getTempstore($entity)->delete($key);
  }

  /**
   * Gets the shared tempstore.
   *
   * @return \Drupal\Core\TempStore\SharedTempStore
   *   The tempstore.
   */
  protected function getTempstore(EntityInterface $entity) {
    return $this->tempStoreFactory->get('edit_plus.entity_storage');
  }

  /**
   * Gets the string to use as the tempstore key.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   An entity.
   *
   * @return string
   *   A unique string representing the entity.
   */
  protected function getKey(EntityInterface $entity) {
    return $entity->getEntityTypeId() . '.' . $entity->id();
  }

}
