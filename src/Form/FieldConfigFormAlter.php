<?php

namespace Drupal\edit_plus\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\edit_plus\EventSubscriber\HandleFieldAttribute;

/**
 * Field config form alter.
 *
 * Edit + field settings.
 */
class FieldConfigFormAlter {

  use StringTranslationTrait;

  /**
   * Implements hook_form_FORM_ID_alter() for the 'field_config_edit' form ID.
   *
   * @param $form
   *   Forms.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *  The form state.
   */
  public function formAlter(&$form, FormStateInterface $form_state) {
    /** @var \Drupal\field_ui\Form\FieldConfigEditForm $form_object */
    $form_object = $form_state->getFormObject();
    /** @var \Drupal\Core\Field\FieldConfigInterface $field */
    $field = $form_object->getEntity();

    $form['edit_plus'] = [
      '#type' => 'details',
      '#title' => t('Edit+'),
      '#open' => TRUE,
      '#tree' => TRUE,
      '#weight' => 23,
    ];
    $form['edit_plus']['disable'] = [
      '#type' => 'checkbox',
      '#title' => t('Disable inline editing for this field'),
      '#default_value' => $field->getThirdPartySetting('edit_plus', 'disable'),
    ];
    $form['edit_plus']['handle'] = [
      '#type' => 'radios',
      '#title' => t('Form Item Handle'),
      '#default_value' => $field->getThirdPartySetting('edit_plus', 'handle') ?? HandleFieldAttribute::getDefault($field) ?? 'form_item',
      '#options' => [
        'form_item' => $this->t('Form Item'),
        'wrapper' => $this->t('Wrapper'),
      ],
      '#description' => $this->t('Select whether the markup on the page that is being edited will be replaced by the entire wrapper or just the form item.'),
      '#states' => [
        'invisible' => [
          ':input[name="edit_plus[disable]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    array_unshift($form['actions']['submit']['#submit'], [$this, 'submit']);
  }

  /**
   * Submit handler.
   */
  public function submit(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $values['third_party_settings']['edit_plus'] = $values['edit_plus'];
    $form_state->setValues($values);
  }

}
