<?php

namespace Drupal\edit_plus\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\edit_plus\EditPlusFormTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\edit_plus\EditPlusTempstoreRepository;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\field_sample_value\SampleValueEntityGeneratorInterface;

/**
 * Inline Entity Form Alter.
 */
class InlineEntityFormAlter implements EntityFormInterface {

  use MessengerTrait;
  use EditPlusFormTrait;
  use StringTranslationTrait;
  use DependencySerializationTrait;

  protected Request $request;

  public function __construct(
    protected EntityDisplayRepositoryInterface $entityDisplayRepository,
    protected EditPlusTempstoreRepository $editPlusTempstoreRepository,
    protected SampleValueEntityGeneratorInterface $fieldGenerator,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EventDispatcherInterface $dispatcher,
    protected ModuleHandlerInterface $moduleHandler,
    protected RendererInterface $renderer,
    protected RequestStack $requestStack,
  ) {
    $this->request = $this->requestStack->getCurrentRequest();
  }

  /**
   * Edit plus inline entity form alter.
   */
  public function formAlter(&$form, FormStateInterface $form_state) {
    $form['actions']['submit']['#ajax'] = [
      'callback' => [$this, 'ajaxSubmit'],
    ];
    // Update a tempstore version instead of saving the entity.
    $save_key = array_search('::save', $form['actions']['submit']['#submit']);
    unset($form['actions']['submit']['#submit'][$save_key]);

    $form['actions']['submit']['#submit'][] = [$this, 'update'];
    $form['actions']['submit']['#value'] = $this->t('Update');
    $form['actions']['submit']['#attributes']['class'][] = 'edit-plus-update-button';

    $this->prepareFormForInlineEditing($form, $form_state);
    array_unshift($form['#process'], [$this, 'dontSetChanged']);
  }

  /**
   * Submit handler that overrides the default entity save behavior and updates
   * an Edit + entity tempstore instead. We do this because saving all the time
   * would create a ton of revisions.
   */
  public function update(array &$form, FormStateInterface $form_state) {
    $entity = $form_state->getFormObject()->getEntity();
    $this->editPlusTempstoreRepository->set($entity);
    Cache::invalidateTags([getCacheTag($entity)]);
    $form_state->setTemporaryValue('updatePage', TRUE);

    $input = $form_state->getUserInput();
    if (!empty($input['empty_field'])) {
      $input['empty_field'] = NULL;
      $form_state->setUserInput($input);
      $form_state->setRebuild(TRUE);
      $form_state->setTemporaryValue('updateForm', TRUE);
    }
  }

  /**
   * Submit button (now tempstore update) AJAX callback.
   */
  public function ajaxSubmit(array &$form, FormStateInterface $form_state) {
    $entity = $form_state->getFormObject()->getEntity();
    $response = new AjaxResponse();

    if ($form_state->getTemporaryValue('updatePage')) {
      $this->updatePage($response, $form, $form_state);
    }
    $input = $form_state->getUserInput();
    if ($form_state->getTemporaryValue('updateForm') || !empty($input['previously_had_errors'])) {
      $this->updateForm($response, $form, $form_state);
    }

    $this->renderMessages($response);
    $this->handleErrors($response, $form, $form_state);

    $response->addCommand(new InvokeCommand(NULL, 'editPlusIsDoneUpdating'));
    return $response;
  }

  /**
   * Don't set changed.
   */
  public function dontSetChanged($element, FormStateInterface $form_state, $form) {
    $input = $form_state->getUserInput();
    // Let the form submission process set the entity changed property. Don't
    // allow it to be set it prematurely.
    unset($input['changed']);
    $form_state->setUserInput($input);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormEntity(array &$form, FormStateInterface $form_state): EntityInterface {
    return $form_state->getFormObject()->getEntity();
  }

  /**
   * {@inheritdoc}
   */
  public function getMainEntity(array &$form, FormStateInterface $form_state): EntityInterface {
    return $this->getFormEntity($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getUserInput(FormStateInterface $form_state) {
    return $form_state->getUserInput();
  }

  /**
   * {@inheritdoc}
   */
  public function updateFormState(array &$form, FormStateInterface $form_state, FieldItemListInterface $field, EntityInterface $entity) {
    $field_name = $field->getName();
    $input = $form_state->getUserInput();
    if (!empty($form[$field_name]['widget']['#type']) && !empty($form[$field_name]['widget']['#key_column']) && $form[$field_name]['widget']['#type'] === 'checkboxes') {
      // Entity reference checkboxes form items don't take the field values as is. Convert them.
      $value = [];
      $key = $form[$field_name]['widget']['#key_column'];
      foreach ($field->getValue() as $v) {
        $value[$v[$key]] = $v[$key];
      }
    } else {
      $value = $field->getValue();
    }
    $input[$field_name] = $value;
    $form_state->setUserInput($input);
    $form_state->setValue($field_name, array_replace($form_state->getValue($field_name), $value));
    $form_state->getFormObject()->setEntity($entity);
    $this->editPlusTempstoreRepository->set($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function entityContent(array &$form, FormStateInterface $form_state, string $view_mode = NULL) {
    $entity = $this->editPlusTempstoreRepository->get($form_state->getFormObject()->getEntity());
    return $this->entityTypeManager->getViewBuilder($entity->getEntityTypeId())->view($entity, $view_mode);
  }

}
