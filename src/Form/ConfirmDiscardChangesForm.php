<?php

namespace Drupal\edit_plus\Form;

use Drupal\Core\Url;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DeleteConfirmationForm.
 */
class ConfirmDiscardChangesForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'discard_changes_confirmation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to discard changes?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('<front>');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Discard changes');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entities = explode('::', $form_state->getFormObject()->requestStack->getCurrentRequest()->get('entities'));
    if (empty($entities)) {
      throw new \InvalidArgumentException('No entities to save.');
    }
    foreach ($entities as $entity) {
      [$entity_type_id, $entity_id] = explode('.', $entity);
      $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
      Cache::invalidateTags([getCacheTag($entity)]);
      \Drupal::service('edit_plus.tempstore_repository')->delete($entity);
    }
  }
}
