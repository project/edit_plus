<?php

namespace Drupal\edit_plus\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\edit_plus\EditPlusMessagesTrait;
use Drupal\edit_plus\EditPlusTempstoreRepository;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Routing\ResettableStackedRouteMatchInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;

/**
 * Entity edit form alter.
 *
 * Alters the regular entity edit pages to add tempstore functionality.
 */
class EntityEditFormAlter {

  use MessengerTrait;
  use EditPlusMessagesTrait;
  use StringTranslationTrait;
  use DependencySerializationTrait;

  public function __construct(
    protected EditPlusTempstoreRepository $tempstoreRepository,
    protected ResettableStackedRouteMatchInterface $routeMatch,
  ) {}

  /**
   * Alter active tempstore entity edit forms.
   *
   * @param $form
   *   Forms.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *  The form state.
   */
  public function formAlter(&$form, FormStateInterface $form_state) {
    if (!$this->isTempstoredEntityEditRoute($form_state)) {
      return;
    }

    $form['actions']['submit']['#submit'][] = [$this, 'clearTempstore'];
    $form['actions']['update'] = [
      '#type' => 'submit',
      '#submit'=> [
        '::submitForm',
        [$this, 'updateTempstore'],
      ],
      '#value' => $this->t('Update'),
      '#weight' => 30,
    ];
    $form['actions']['discard'] = [
      '#type' => 'submit',
      '#submit'=> [[$this, 'clearTempstore']],
      '#value' => $this->t('Discard changes'),
      '#weight' => 50,
    ];
  }

  /**
   * Update tempstore.
   *
   * Updates the Edit+ tempstore for the currently edited entity.
   *
   * @param $form
   *   The entity form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function updateTempstore(&$form, FormStateInterface $form_state) {
    $this->tempstoreRepository->set($form_state->getFormObject()->getEntity());
    $this->clearUnsavedChangesMessage();
  }

  /**
   * Clear tempstore.
   *
   * Clears the Edit+ tempstore for the currently edited entity.
   *
   * @param $form
   *   The entity form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function clearTempstore(&$form, FormStateInterface $form_state) {
    $this->tempstoreRepository->delete($form_state->getFormObject()->getEntity());
    $this->clearUnsavedChangesMessage();
  }

  /**
   * Is tempstore entity edit route.
   *
   * Determines if we are editing an entity that has an Edit + tempstore version.
   *
   * @return bool
   */
  protected function isTempstoredEntityEditRoute(FormStateInterface $form_state) {
    $actively_used_tempstore_entities = edit_plus_active_tempstore_entities();
    $form_object = $form_state->getFormObject();
    if (!empty($actively_used_tempstore_entities) && $form_object instanceof ContentEntityForm) {
      $entity = $form_object->getEntity();
      foreach ($actively_used_tempstore_entities as $tempstore_entity) {
        // Only form alter the edit page of the tempstore entity.
        if (
          $entity->getEntityTypeId() === $tempstore_entity->getEntityTypeId() &&
          $entity->id() === $tempstore_entity->id() &&
          $entity->toUrl('edit-form')->getRouteName() === $this->routeMatch->getRouteName()
        ) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

}
