<?php declare(strict_types = 1);

namespace Drupal\edit_plus\Controller;

use Drupal\edit_plus\Ui;
use Drupal\Core\Form\FormState;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\edit_plus\EditPlusFormTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\edit_plus\EditPlusTempstoreRepository;
use Psr\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\field_sample_value\SampleValueEntityGeneratorInterface;
use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;

/**
 * Builds the entity form for the given entity.
 */
final class MultipleEntityFormController extends ControllerBase {

  use EditPlusFormTrait;
  use StringTranslationTrait;

  public function __construct(
    protected Request $request,
    protected EventDispatcherInterface $dispatcher,
    protected ArgumentResolverInterface $argumentResolver,
    protected EditPlusTempstoreRepository $tempstoreRepository,
    protected SampleValueEntityGeneratorInterface $fieldGenerator,
    protected EntityDisplayRepositoryInterface $entityDisplayRepository,
  ) {}

  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('event_dispatcher'),
      $container->get('http_kernel.controller.argument_resolver'),
      $container->get('edit_plus.tempstore_repository'),
      $container->get('field_sample_value.generator'),
      $container->get('entity_display.repository'),
    );
  }

  /**
   * Builds the response.
   */
  public function entityForm(Request $request, $entity_type, EntityInterface $entity) {
    // Build an entity form.
    $form_object = $this->entityTypeManager()->getFormObject($entity->getEntityTypeId(), 'default');
    $entity = $this->tempstoreRepository->get($entity);
    $form_object->setEntity($entity);

    // This is an excerpt from FormController.
    // Add the form and form_state to trick the getArguments method of the
    // controller resolver.
    $form_state = (new FormState())->setFormState(['edit_plus_form' => TRUE]);
    $request->attributes->set('form', []);
    $request->attributes->set('form_state', $form_state);
    $args = $this->argumentResolver->getArguments($request, [$form_object, 'buildForm']);
    $request->attributes->remove('form');
    $request->attributes->remove('form_state');

    // Remove $form and $form_state from the arguments, and re-index them.
    unset($args[0], $args[1]);
    $form_state->addBuildInfo('args', array_values($args));

    // @see InlineEntityFormAlter for modifications.
    $unwrapped_form = $this->formBuilder()->buildForm($form_object, $form_state);
    $form = $this->wrapForm($unwrapped_form, $entity);

    if (empty($request->get('ajaxReturnForm'))) {
      // The form is being built because the AJAX submit button has been clicked by JS.
      // @see script.js editor.ui.focusTracker.on()
      return $form;
    }

    $response = new AjaxResponse();
    $response->addCommand(new AppendCommand('#navigation-plus-right-sidebar', $form));

    return $response;
  }

  public function entityContent(array &$form, FormStateInterface $form_state, string $view_mode = NULL) {}

}
