<?php

declare(strict_types=1);

namespace Drupal\edit_plus\Plugin\Tool;

use Drupal\navigation_plus\Attribute\Tool;
use Drupal\navigation_plus\ToolPluginBase;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implementation of the tool.
 */
#[Tool(
  id: 'edit_plus',
  label: new TranslatableMarkup('Edit'),
  weight: 40,
)]
final class EditPlus extends ToolPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getIconsPath(): array {
    $path = $this->extensionList->getPath('edit_plus');
    return [
      'mouse_icon' => "/$path/assets/text-mouse.svg",
      'toolbar_button_icons' => [
        'edit_plus' => "/$path/assets/pencil.svg",
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function addAttachments(array &$attachments): void {
    $attachments['library'][] = 'edit_plus/library';
    // Get libraries required for CKEditors.
    $formats = \Drupal::service('entity_type.manager')->getStorage('filter_format')->loadMultiple();
    $editor_attachments = \Drupal::service('plugin.manager.editor')->getAttachments(array_keys($formats));
    $attachments = NestedArray::mergeDeep($attachments, $editor_attachments);
  }

}
