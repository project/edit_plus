<?php

namespace Drupal\edit_plus\Element;

use Drupal\filter\Element\TextFormat;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;

/**
 * Inline CKEditor element.
 *
 * @FormElement("inline_textarea")
 */
class InlineTextarea extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#process' => [
        [static::class, 'processFormat'],
      ],
      '#base_type' => 'textarea',
      '#theme_wrappers' => ['text_format_wrapper'],
      '#theme' => 'inline_textarea',
    ];
  }

  public static function processFormat(&$element, FormStateInterface $form_state, &$complete_form) {
    $element = TextFormat::processFormat($element, $form_state, $complete_form);
    unset($element['#theme']);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if (is_array($input) && isset($input['value'])) {
      return $input['value'];
    }
    // Use the #default_value.
    return NULL;
  }

}
