<?php

namespace Drupal\edit_plus\Ajax;

use Drupal\Core\Ajax\CommandInterface;
use Drupal\Core\Ajax\CommandWithAttachedAssetsTrait;

class UpdateMarkup implements CommandInterface {

  use CommandWithAttachedAssetsTrait;

  /**
   * @param string $selector
   *   A CSS selector.
   * @param array $content
   *   The content that will be updated.
   * @param string|null $updatedElementId
   *   The rendered element to be updated.
   */
  public function __construct(
    protected string $selector,
    protected array $content,
    protected ?string $updatedElementId = NULL,
  ) {}

  public function render() {
    return [
      'command' => 'invoke',
      'selector' => $this->selector,
      'method' => 'EditPlusUpdateMarkup',
      'args' => [$this->selector, $this->getRenderedContent(), $this->updatedElementId],
    ];
  }

}
