<?php

namespace Drupal\edit_plus\Ajax;

use Drupal\Core\Ajax\CommandInterface;
use Drupal\Core\Ajax\CommandWithAttachedAssetsTrait;

class ShowFieldWithErrors implements CommandInterface {

  use CommandWithAttachedAssetsTrait;

  /**
   * @param string $selector
   *   A CSS selector.
   */
  public function __construct(
    protected string $selector,
    protected string $handle,
  ) {}

  public function render() {
    return [
      'command' => 'invoke',
      'method' => 'ShowFieldWithErrors',
      'args' => [$this->selector, $this->handle],
    ];
  }

}
