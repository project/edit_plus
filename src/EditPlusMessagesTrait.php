<?php

namespace Drupal\edit_plus;

trait EditPlusMessagesTrait {

  /**
   * Clear unsaved changes message.
   */
  public function clearUnsavedChangesMessage() {
    // @todo Remove this and update things that call it?
    // now we are removing all of these messages in lb_plus_preprocess_status_messages
    $messenger = \Drupal::messenger();
    $all_messages = $messenger->all();
    if (!empty($all_messages['warning'])) {
      foreach ($all_messages['warning'] as $key => $warning) {
        if ((string) t('You have unsaved changes.') === (string) $warning) {
          $removed_warning = TRUE;
          unset($all_messages['warning'][$key]);
        }
      }
      if (isset($removed_warning)) {
        $messenger->deleteByType('warning');
        if (!empty($all_messages['warning'])) {
          foreach ($all_messages['warning'] as $warning) {
            $messenger->addWarning((string) $warning);
          }
        }
      }
    }
  }

}
