<?php

namespace Drupal\edit_plus_lb;

use Symfony\Component\DependencyInjection\Reference;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Defines a service provider for the LB Plus Edit Plus module.
 */
class EditPlusLbServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->hasDefinition('edit_plus.tempstore_repository')) {
      $definition = $container->getDefinition('edit_plus.tempstore_repository');
      $definition->setClass(EditPlusLbTempstoreRepository::class)
        ->addArgument(new Reference('layout_builder.tempstore_repository'))
        ->addArgument(new Reference('plugin.manager.layout_builder.section_storage'));
    }
  }

}
