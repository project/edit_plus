<?php

namespace Drupal\edit_plus_lb\Entity;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay as BaseEntityViewDisplay;

/**
 * Provides an entity view display entity that has a layout.
 */
class LayoutBuilderEntityViewDisplay extends BaseEntityViewDisplay {

  /**
   * {@inheritdoc}
   */
  protected function buildSections(FieldableEntityInterface $entity) {
    $build = parent::buildSections($entity);
    $build['#cache']['contexts'][] = 'user.permissions';
    if (!\Drupal::currentUser()->hasPermission('access inline editing')) {
      return $build;
    }

    $contexts = $this->getContextsForEntity($entity);
    $label = new TranslatableMarkup('@entity being viewed', [
      '@entity' => $entity->getEntityType()->getSingularLabel(),
    ]);
    $contexts['layout_builder.entity'] = EntityContext::fromEntity($entity, $label);

    $cacheability = new CacheableMetadata();
    $storage = $this->getSectionStorage()->findByContext($contexts, $cacheability);

    if ($storage) {
      foreach ($storage->getSections() as $delta => $section) {
        // Attribute the rendered sections so that the Edit + JS can make sense
        // of the page.
        $build[$delta]['#attributes']['data-layout-builder-section-delta'] = $delta;
        $build[$delta]['#attributes']['data-layout-builder-section-storage-type'] = $storage->getStorageType();
        $build[$delta]['#attributes']['data-layout-builder-section-storage-id'] = $storage->getStorageId();
      }
    }
    return $build;
  }

  protected function getSectionStorage() {
    return \Drupal::service('plugin.manager.layout_builder.section_storage');
  }

}
