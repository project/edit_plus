<?php

namespace Drupal\edit_plus_lb\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Render\Element;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\edit_plus\EditPlusFormTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Block\BlockPluginInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\edit_plus\Form\EntityFormInterface;
use Drupal\block_content\BlockContentInterface;
use Drupal\block_content\BlockContentUuidLookup;
use Drupal\edit_plus\EditPlusTempstoreRepository;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\PluginFormFactoryInterface;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\layout_builder\Plugin\Block\InlineBlock;
use Drupal\block_content\Plugin\Block\BlockContentBlock;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\layout_builder\LayoutTempstoreRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\field_sample_value\SampleValueEntityGeneratorInterface;
use Drupal\layout_builder\Form\UpdateBlockForm as UpdateBlockFormBase;

/**
 * Extends the UpdateBlockForm to add Edit + integrations.
 */
class UpdateBlockForm extends UpdateBlockFormBase implements EntityFormInterface {

  use EditPlusFormTrait;

  public function __construct(
    protected LayoutTempstoreRepositoryInterface $layout_tempstore_repository,
    protected EntityDisplayRepositoryInterface $entityDisplayRepository,
    protected EditPlusTempstoreRepository $editPlusTempstoreRepository,
    protected SampleValueEntityGeneratorInterface $fieldGenerator,
    protected PluginFormFactoryInterface $plugin_form_manager,
    protected ContextRepositoryInterface $context_repository,
    protected BlockContentUuidLookup $blockContentUuidLookup,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected ModuleHandlerInterface $moduleHandler,
    protected EventDispatcherInterface $dispatcher,
    protected RendererInterface $renderer,
    protected Request $request,
    $block_manager,
    $uuid,
  ) {
    parent::__construct($layout_tempstore_repository, $context_repository, $block_manager, $uuid, $plugin_form_manager);
  }

  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('layout_builder.tempstore_repository'),
      $container->get('entity_display.repository'),
      $container->get('edit_plus.tempstore_repository'),
      $container->get('field_sample_value.generator'),
      $container->get('plugin_form.factory'),
      $container->get('context.repository'),
      $container->get('block_content.uuid_lookup'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('event_dispatcher'),
      $container->get('renderer'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('plugin.manager.block'),
      $container->get('uuid'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SectionStorageInterface $section_storage = NULL, $delta = NULL, $region = NULL, $uuid = NULL) {
    $form = parent::buildForm($form, $form_state, $section_storage, $delta, $region, $uuid);
    $form_state->set('section_storage', $section_storage);

    $form['actions']['submit']['#attributes']['class'][] = 'edit-plus-update-button';
    $form['actions']['submit']['#attributes']['class'][] = 'edit-plus-hidden';

    $form['settings']['block_form']['#process'][] = [$this, 'processBlockForm'];
    $form['#after_build'][] = [$this, 'autoSubmitProperties'];
    $form['#after_build'][] = [$this, 'wrapFormAfterBuild'];

    $this->attributeLabel($form, $form_state);

    return $form;
  }

  /**
   * Process callback for the block form.
   *
   * @param array $element
   *   The block plugin form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state
   *
   * @return array
   *   The block plugin form.
   */
  public function processBlockForm(array $element, FormStateInterface $form_state) {
    $this->prepareFormForInlineEditing($element, $form_state);

    // Submit the form after adding an empty field to the page.
    if (!empty($element['empty_fields'])) {
      foreach (Element::children($element['empty_fields']) as $field_name) {
        $element['empty_fields'][$field_name]['#submit'][] = [$this, 'submitForm'];
      }
    }

    return $element;
  }

  /**
   * After build callback.
   */
  public function wrapFormAfterBuild(array &$element, FormStateInterface $form_state) {
    $block = $this->getFormEntity($element, $form_state);
    if ($this->getRequest()->get('ajaxReturnForm')) {
      // The form is being built because the AJAX submit button has been clicked by JS.
      // @see script.js editor.ui.focusTracker.on()
      return $this->wrapForm($element, $block);
    }
    return $element;
  }

  /**
   * Validation handler.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $subform_state = SubformState::createForSubform($form['settings'], $form, $form_state);
    $this->getPluginForm($this->block)->validateConfigurationForm($form['settings'], $subform_state);
  }

  /**
   * Submit handler.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Clear the page cache.
    $parent_entity = $this->getMainEntity($form, $form_state);
    Cache::invalidateTags([getCacheTag($parent_entity)]);
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxSubmit(array &$form, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) {
      $response = new AjaxResponse();
      $this->renderMessages($response);
      $this->handleErrors($response, $form, $form_state);
    }
    else {
      $response = $this->successfulAjaxSubmit($form, $form_state);
    }
    return $response;
  }

  /**
   * Submission AJAX callback.
   */
  protected function successfulAjaxSubmit(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $this->renderMessages($response);
    $this->updatePage($response, $form, $form_state);

    // Check if a field was just emptied.
    $input = $form_state->getUserInput();
    if (!empty($input['settings']['block_form']['empty_field']) || !empty($input['previously_had_errors'])) {
      $this->updateForm($response, $form, $form_state);
    }

    $response->addCommand(new InvokeCommand(NULL, 'editPlusIsDoneUpdating'));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function updateFormState(array &$form, FormStateInterface $form_state, FieldItemListInterface $field, EntityInterface $entity) {
    $field_name = $field->getName();
    $input = $form_state->getUserInput();
    $input['settings']['block_form'][$field_name] = $field->getValue();
    $form_state->setUserInput($input);
    $form_state->setValue(['settings', 'block_form', $field_name], array_replace($form_state->getValue(['settings', 'block_form', $field_name]), $field->getValue()));
  }

  /**
   * {@inheritdoc}
   */
  public function getUserInput(FormStateInterface $form_state) {
    $input = $form_state->getUserInput();
    if (!empty($input['settings']['block_form'])) {
      $input = $input['settings']['block_form'];
    }
    return $input;
  }

  /**
   * {@inheritdoc}
   */
  public function entityContent(array &$form, FormStateInterface $form_state, string $view_mode = NULL) {
    $component = $this->getComponent($form, $form_state);
    $contexts = ['layout_builder.entity' => EntityContext::fromEntity($this->getMainEntity($form, $form_state))];
    return [
      // Add a wrapper so Drupal.behaviors.EditPlusEnable can find the
      // entity wrapper which is context in this case.
      // @todo Could you do an alternative once like in inline-editor.js attachEditor instead?
      '#type' => 'container',
      'component' => $component->toRenderArray($contexts),
    ];
  }

  /**
   * Get section component.
   *
   * @param array $form
   *   The Inline Block plugin form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\layout_builder\SectionComponent
   *   The section component for the block entity being edited.
   */
  protected function getComponent(array &$form, FormStateInterface $form_state) {
    $args = $form_state->getBuildInfo()['args'];
    $section_delta = $args[1];
    $component_uuid = $args[3];
    $component = $form_state->get('section_storage')->getSection($section_delta)->getComponent($component_uuid);
    return $component;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormEntity(array &$form, FormStateInterface $form_state): EntityInterface {
    $component = $this->getComponent($form, $form_state);
    $block_content = $this->getBlockContent($component->getPlugin());

    return $block_content;
  }

  public function getMainEntity(array &$form, FormStateInterface $form_state): EntityInterface {
    $entity = $form_state->get('section_storage')->getContextValue('entity');
    return $entity;
  }


  /**
   * Get block content.
   *
   * Duplicated from sectionStorageHandler.
   *
   * @param \Drupal\Core\Block\BlockPluginInterface $block_plugin
   *   The block plugin.
   *
   * @return \Drupal\block_content\BlockContentInterface|null
   *   The block content entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getBlockContent(BlockPluginInterface $block_plugin): ?BlockContentInterface {
    $configuration = $block_plugin->getConfiguration();
    $block_content = NULL;
    // Load the block content.
    if ($block_plugin instanceof InlineBlock) {
      if (!empty($configuration['block_serialized'])) {
        $block_content = unserialize($configuration['block_serialized']);
      }
      elseif (!empty($configuration['block_revision_id'])) {
        $block_content = $this->entityTypeManager->getStorage('block_content')->loadRevision($configuration['block_revision_id']);
      }
    }
    if ($block_plugin instanceof BlockContentBlock) {
      $uuid = $block_plugin->getDerivativeId();
      if ($id = $this->blockContentUuidLookup->get($uuid)) {
        $block_content = $this->entityTypeManager->getStorage('block_content')->load($id);
      }
    }
    return $block_content;
  }

  protected function attributeLabel(array &$form, FormStateInterface $form_state) {
    $entity = $this->getFormEntity($form, $form_state);
    $form_item_id = sprintf('%s::%s::%s::%s::%s', $entity->getEntityTypeId(), edit_plus_entity_identifier($entity), 'label', 0, 'block_property');
    $form['settings']['label']['#wrapper_attributes']['data-edit-plus-form-item-id'] = $form_item_id;
    $form['settings']['label']['#wrapper_attributes']['class'][] = 'edit-plus-form-item';
    $form['settings']['label']['#wrapper_attributes']['class'][] = 'edit-plus-hidden';
    $form['settings']['label_wrapper'] = [
      '#type' => 'container',
      'label' => $form['settings']['label'],
      '#attributes' => [
        'data-edit-plus-form-item-wrapper-id' => sprintf('%s::%s::%s::%s', $entity->getEntityTypeId(), edit_plus_entity_identifier($entity), 'label', 'block_property'),
        'data-edit-plus-form-item-widget' => 'string_textfield',
        'class' => ['edit-plus-form-item-wrapper'],
      ],
      // This ensures the value is stored under $values['settings']['label']
      // even though we added a wrapper.
      '#parents' => ['settings'],
    ];
    unset($form['settings']['label']);

  }

  //

  /**
   * Auto-submit properties.
   *
   * Attribute block properties so they can be auto-saved when changed in the sidebar.
   *
   * @param array $form
   *   The block form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The block form.
   */
  public function autoSubmitProperties(array &$form, FormStateInterface $form_state) {
    $block_properties = array_diff(Element::children($form['settings']), ['label', 'provider', 'block_form', 'context_mapping']);
    foreach ($block_properties as $property) {
      $form_item =& $form['settings'][$property];
      if ($form_item['#type'] !== 'item') {
        $form_item['#attributes']['data-edit-plus-auto-submit'] = '';
      }
    }
    return $form;
  }

}
