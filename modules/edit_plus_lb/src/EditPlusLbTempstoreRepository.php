<?php

namespace Drupal\edit_plus_lb;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\TempStore\SharedTempStoreFactory;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\layout_builder\LayoutEntityHelperTrait;
use Drupal\layout_builder\LayoutTempstoreRepositoryInterface;
use Drupal\layout_builder\Entity\LayoutEntityDisplayInterface;
use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay;
use Drupal\layout_builder\Plugin\SectionStorage\DefaultsSectionStorage;
use Drupal\layout_builder\SectionStorage\SectionStorageManagerInterface;
use Drupal\edit_plus\EditPlusTempstoreRepository as EditPlusTempstoreRepositoryBase;

/**
 * Provides a mechanism for loading layouts from tempstore.
 */
class EditPlusLbTempstoreRepository extends EditPlusTempstoreRepositoryBase {

  use LayoutEntityHelperTrait {
    getSectionStorageForEntity as lbGetSectionStorageForEntity;
  }

  public function __construct(
    protected SharedTempStoreFactory $tempStoreFactory,
    protected LayoutTempstoreRepositoryInterface $layoutTempstoreRepository,
    protected SectionStorageManagerInterface $section_storage_manager,
  ) {
    $this->sectionStorageManager = $this->section_storage_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function get(EntityInterface $entity) {
    if (!$this->editPlusIsLayoutCompatibleEntity($entity)) {
      return parent::get($entity);
    }
    return $this->getLbTempstoreEntity($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function has(EntityInterface $entity) {
    if (!$this->editPlusIsLayoutCompatibleEntity($entity)) {
      return parent::has($entity);
    }

    return !empty($this->getLbTempstoreEntity($entity));
  }

  /**
   * {@inheritdoc}
   */
  public function set(EntityInterface $entity) {
    if (!$this->editPlusIsLayoutCompatibleEntity($entity)) {
      parent::set($entity);
      return;
    }
    $section_storage = $this->getSectionStorageForEntity($entity);
    $this->layoutTempstoreRepository->set($section_storage);
  }

  /**
   * {@inheritdoc}
   */
  public function delete(EntityInterface $entity) {
    if (!$this->editPlusIsLayoutCompatibleEntity($entity)) {
      parent::delete($entity);
      return;
    }
    $section_storage = $this->getSectionStorageForEntity($entity);
    $this->layoutTempstoreRepository->delete($section_storage);
  }

  private function editPlusIsLayoutCompatibleEntity(EntityInterface $entity) {
    return $entity instanceof ContentEntityInterface && $this->isLayoutCompatibleEntity($entity);
  }

  /**
   * Get section storage for entity.
   *
   * Always load an override section storage because even when we would get a
   * default section storage from LayoutEntityHelperTrait->getSectionStorageForEntity
   * we actually just want to save a new override section storage.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The layout builder managed entity.
   *
   * @return \Drupal\layout_builder\SectionStorageInterface|null
   *   The section storage for the entity.
   */
  private function getSectionStorageForEntity(EntityInterface $entity): ?SectionStorageInterface {
    // This is a copy and tweak of LayoutEntityHelperTrait. It finds the
    // section storage by context.
    $view_mode = 'full';
    if ($entity instanceof LayoutEntityDisplayInterface) {
      $contexts['display'] = EntityContext::fromEntity($entity);
      $contexts['view_mode'] = new Context(new ContextDefinition('string'), $entity->getMode());
    }
    else {
      $contexts['entity'] = EntityContext::fromEntity($entity);
      if ($entity instanceof FieldableEntityInterface) {
        $display = EntityViewDisplay::collectRenderDisplay($entity, $view_mode);
        if ($display instanceof LayoutEntityDisplayInterface) {
          $contexts['display'] = EntityContext::fromEntity($display);
        }
        // Fall back to the actually used view mode (e.g. full > default).
        $contexts['view_mode'] = new Context(new ContextDefinition('string'), $display->getMode());
      }
    }
    $section_storage = $this->sectionStorageManager->findByContext($contexts, new CacheableMetadata());
    // Create an override section storage if we have a default section storage.
    if ($section_storage instanceof DefaultsSectionStorage) {
      $view_mode = 'full';
      $contexts['entity'] = EntityContext::fromEntity($entity);
      $view_mode = LayoutBuilderEntityViewDisplay::collectRenderDisplay($entity, $view_mode)->getMode();
      $contexts['view_mode'] = new Context(new ContextDefinition('string'), $view_mode);

      $override_section_storage = $this->sectionStorageManager->load('overrides', $contexts);
      if (empty($override_section_storage)) {
        return NULL;
      }
      // Copy the sections from the default to the override.
      $sections = $section_storage->getSections();
      if (!empty($sections)) {
        foreach ($sections as $section) {
          $override_section_storage->appendSection($section);
        }
      }
      $section_storage = $override_section_storage ;
    }
    return $section_storage;
  }

  /**
   * Get Layout Builder tempstore entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to retrieve.
   *
   * @return EntityInterface|null
   *    The Layout Builder tempstore version of the entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\ContextException
   */
  private function getLbTempstoreEntity(EntityInterface $entity) {
    $section_storage = $this->getSectionStorageForEntity($entity);

    return !empty($section_storage) ? $this->layoutTempstoreRepository->get($section_storage)->getContextValue('entity') : NULL;
  }

}
