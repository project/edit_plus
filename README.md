# Edit +
Edit+ adds inline editing functionality. It does so by loading the entity form in the sidebar. When you click to edit a page element, it is replaced by the hidden form element. Doing it in this way means that all the back end is the same as the `/node/1/edit` page, so form_alters and what not still work. 👍🏻

# Troubleshooting
Edit+ works by loading the relevant entity form into the sidebar as a "context" sidebar. The form items are hidden initially, but when an editable element is clicked on the page the form item is swapped into place for inline editing.

### Inline Editing z-index
If the inline editing z-index is undesirable for your theme override it with. Since the inline editor is visually moved out of the sidebar, it inherits whatever the sidebar z-index is.
```css
.navigation-plus-sidebar {
  z-index: calc(var(--navigation-plus-right-sidebar-z-index, 10) + 1);;
}
```

### Pop out width
Say you have an inline editable element, like a paragraph or a media item, and the form item widget is too big to fit inline. You can form alter a `data-pop-out-width=618` attribute onto the form item wrapper and when ever the space to edit in is smaller than the form item will show up in a pop out. The pop out width defaults to `200px`.

### Field templates
In order for Edit+ to be able to edit a field, the field template must have the wrapper and item attributes printed to the page. All core field templates do something like this to include the wrapper and item attributes. Ensure your field templates do too if you want them to be inline editable.
```twig
<div{{ attributes }}>
  {% for item in items %}
    <div{{ item.attributes }}>
      {{ item }}
    </div>
  {% endfor %}
</div>
```
If you need to access the things inside the item you need a little ternary.
```twig
<div{{ attributes }}>
  {% for item in items %}
    {% set item = item.content.edit_plus_wrapper is defined ? item.content.edit_plus_wrapper : item %}
    {% set item_attributes = item.attributes is defined ? item.attributes : create_attribute(item['#attributes']) %}
    <div{{ item_attributes }}>
      {{ item[0]['start']['time']['#markup'] }}
    </div>
  {% endfor %}
</div>
```
Going into a node template and directly outputting a field with twig like the following will prevent the Edit+ UI from working since the field markup will not be attributed.
```twig
# Don't do this! Use a field template instead.
{{ content.field_event_date[0]['start']['time']['#markup'] }}
{{ content|without('field_event_date') }}
```
Instead, move this kind of stuff to a field template.

### Messages
Massages are appended to the `<div data-drupal-messages-fallback></div>` element. Ensure your pages have one of those.
